from .base import MediathekDownload
from mediathek_dl.mediathek.config import Config

import re
import os
import sys
import urllib.request
import xml.etree.ElementTree as ET

class ZDFNoDownload(Exception): pass

class ZDFDownload(MediathekDownload):
  def __init__(self, video_id):
    self.channel = 'zdf'
    self.path_broadcast_db = 'zdf.ini'
    self.base_addr    = "http://www.zdf.de/ZDFmediathek/"
    self.xml_addr_ext = "xmlservice/web/beitragsDetails?ak=web&id="
    self.html_addr    = self.base_addr + 'beitrag/video/' + video_id
    self.parsed_xml = False
    super().__init__(video_id)

  def set_video_id(self, video_id):
    self.video_id  = video_id
    self.html_addr = self.base_addr + 'beitrag/video/' + video_id

    self.parsed_xml = False

    self.more_info_at = None
    self.short_info   = None
    self.title        = None
    self.channel_ext  = None 
    self.length       = None 
    self.airtime      = None 
    self.timetolive   = None 
    self.fsk          = None 
    self.streams      = None 

    self.xml_output_file = "files/zdf/" + self.video_id + ".xml"
    self.img_output_file  = "files/zdf/" + self.video_id + ".jpg"

  def save_video_data(self):
    self.open_broadcast_db()
    self.files = [ self.xml_output_file ]
    super().save_video_data()

  def download_xml(self):
    url_xml = self.base_addr + self.xml_addr_ext + self.video_id
    os.makedirs("files/zdf", exist_ok=True)
    urllib.request.urlretrieve( url_xml, self.xml_output_file )

  def delete_streamfile(self): self.delete_xml()

  def delete_xml(self): os.remove( self.xml_output_file )

  def parse_xml(self):
    if self.parsed_xml: return True
    if not os.path.exists( self.xml_output_file ):
      self.download_xml()

    xml_tree = ET.parse( self.xml_output_file )
    xml_root = xml_tree.getroot()

    if xml_root[0][0].text != "ok":
      print("Video Id ist ungülig!")
      print("Möglicherweise existiert die Sendung nicht mehr in der Mediathek.")
      sys.exit(56) # what error code to return ?

    node_video = xml_root.find('video')

    context = node_video.find('context')
    if context: self.more_info_at = context[0].text

    info =  node_video.find('information')
    self.title      = info[0].text
    self.short_info = info[1].text

    details = node_video.find('details')
    self.channel_ext = details.find('channel').text
    self.length      = details.find('length').text
    self.airtime     = details.find('airtime').text
    self.timetolive  = details.find('timetolive').text
    self.fsk         = details.find('fsk').text

    node_streams = node_video.find('formitaeten')

    metafstream = None
    nrodlstream = None

    self.streams = { 'low' : [], 'med' : [],
                     'high' : [], 'veryhigh' : [], 'hd': [] }

    for node_stream in node_streams.findall('formitaet'):
      streams = self.parse_xml_formitaet( node_stream )
      for stream in streams:
        if self.has_fsk(stream):
          return False
        is_metaf_stream = self.is_metaf_stream(metafstream, stream)
        if is_metaf_stream: metafstream = stream
        nrodlstream = self.is_nrodl_stream(nrodlstream, stream)
        #if not self.has_video_url( self.streams, stream['url'] ): # slow !!!
        #  self.streams[stream['quality']].append(stream)
        cfg = Config()
        use_meta_stream = cfg.check_flag('zdf', 'use_metafile_generator_stream')
        if use_meta_stream or not is_metaf_stream:
          self.streams[stream['quality']].append(stream)

    images = {}
    node_images = node_video.findall('./teaserimages/teaserimage')
    for node_image in node_images:
      size = int( re.sub('x.*', '', node_image.attrib['key']) )
      images[size] = node_image.text
    self.image_url = images[sorted(images.keys())[-1]]


    if len(node_streams): self.metaf_to_nrodl(metafstream, nrodlstream)
    else                : self.streams = None

    self.parsed_xml = True
    return True

  def is_metaf_stream(self, have_metaf, stream):
    if stream['quality'] != 'veryhigh': return False
    if re.search('metafilegenerator', stream['url']): return stream

  def is_nrodl_stream(self, have_nrodl, stream):
    if not have_nrodl and re.match('http://(nrodl|rodl).zdf.de', stream['url']):
      return stream
    return have_nrodl

  def metaf_to_nrodl(self, metafs, nrodls):
    if nrodls:
      nrodl = re.match('(.*/)[^/]*$', nrodls['url']).group(1)
      metafs['url'] = re.sub('^.*/([^/]*)$', nrodl + '\\1', metafs['url'] )

  # this is too slow !
  def has_video_url(self,streams,url, keys=['low', 'med', 'high', 'veryhigh'] ):
    if keys == None:
      for stream in streams:
        if stream['url'] == url: return True
    else:
      for key in keys:
        for stream in streams[key]:
          if stream['url'] == url: return True
    return False

  def parse_xml_formitaet( self, node_stream ):
    stream = {}

    stream['url']      = node_stream.find('url').text
    if re.search('(m3u8|f4m|3gp)$', stream['url']) != None: return []
    if re.search('meta$', stream['url']) != None:
      stream['url'] = self.get_meta(stream['url'])

    stream['quality']  = node_stream.find('quality').text

    node_ratio = node_stream.find('ratio')
    if node_ratio != None: stream['ratio'] = node_ratio.text

    node_height = node_stream.find('heigth')
    if node_height != None:
      stream['heigth'] = node_height.text
      stream['width']  = node_stream.find('width').text

    node_vbr = node_stream.find('videoBitrate')
    if node_vbr != None: stream['vbr'] = node_vbr.text
    else:                stream['vbr'] = 0

    stream['abr']      = node_stream.find('audioBitrate').text
    stream['filesize'] = node_stream.find('filesize').text

    if re.search('smil$', stream['url']) != None:
      return self.get_smil(stream['url'], stream['ratio'])

    return [stream]

  def get_meta(self, url):
    response = urllib.request.urlopen( url )
    meta_root = ET.fromstring(response.read())
    return meta_root[0].text

  def get_smil(self, url, ratio):
    response = urllib.request.urlopen( url )
    smil_root = ET.fromstring(response.read())

    default_namespace = re.search('{(.*)}', smil_root.tag).group(1)
    namesp = { 'ns' : default_namespace }

    app  = smil_root.find(".//ns:param[@name='app']",  namesp).attrib['value']
    host = smil_root.find(".//ns:param[@name='host']", namesp).attrib['value']

    streams = []
    for video in smil_root.findall('.//ns:video', namesp):
      stream = { 'app':app, 'host':host }
      stream['url'] = video.attrib['src']
      stream['quality'] = video[0].attrib['value']
      stream['vbr'] = video.attrib['system-bitrate']
      streams.append(stream)

    return streams

  def filter_stream(self, url):
    if re.search('(m3u8|f4m|3gp)$'): return True
    return False
    
  def print_video_info(self):
    if not self.parsed_xml: self.parse_xml()

    print( self.title + '\n' )

    nodl = '-- Achtung: Zu dieser Sendung wurden keine Downloads gefunden --\n'
    if not self.streams: print(nodl)

    print( "Id:         %s"   % self.video_id )
    print( "Sender:     %s"   % self.channel_ext )
    print( "Zeit:       %s"   % self.airtime )
    print( "Dauer:      %s"   % self.length )
    print( "Fsk:        %s\n" % self.fsk )

    print( "Kurzinfo:\n%s\n" % self.short_info )

    print( "Links:" )
    if self.more_info_at:
      print( "· Weitere Infos unter:\n  %s" % self.more_info_at )
    print( "· Seite in der ZDF Mediathek:\n  %s" % self.html_addr )

  def print_video_streams(self):
    if not self.parsed_xml:
      if not self.parse_xml(): return False

    if not self.streams: return self.no_streams_found()

    if   len(self.streams['hd']):       key = 'hd'
    elif len(self.streams['veryhigh']): key = 'veryhigh'
    elif len(self.streams['high']):     key = 'high'
    elif len(self.streams['med']):      key = 'med'
    elif len(self.streams['low']):      key = 'low'
    else: return self.no_streams_found()

    self.streams[key].sort(key=lambda x: int(x['vbr']), reverse=True)

    self.print_video_stream(self.streams[key][0])
    for i in range(1,len(self.streams[key])):
      url = self.streams[key][i]['url']
      if 'host' in self.streams[key][i]:
        if self.has_video_url( self.streams[key][i+1:], url, None ):
          continue
      print()
      self.print_video_stream(self.streams[key][i])

    return True

  def print_video_stream(self, stream):
    print("Video Bitrate: %s" % stream['vbr'])
    if 'abr' in stream: print("Audio Bitrate: %s" % stream['abr'])

    print("Video Path:    %s" % stream['url'])

    if 'host' in stream: print("Host:          %s" % stream['host'])
    if 'app' in stream:  print("App:           %s" % stream['app'])

  def no_streams_found(self):
    print("Es wurden keine Downloads für das Video gefunden.")
    print("Wird das Video erst später freigeschaltet (wegen FSK)?")
    print("Ist die Sendung noch in der Mediathek vorhanden und abspielbar?")
    print("(Sendungen aus der ZDF Mediathek sind desöfteren defekt.)")
    print("Link zur Sendung in der ZDF Mediathek:\n%s" % self.html_addr)
    return MediathekDownload.DOWNLOAD_NO_STREAM

  def get_video_streams_for_download(self, streams):
    if not self.parsed_xml:
      if not self.parse_xml():
        return MediathekDownload.DOWNLOAD_NO_STREAM

    if not self.streams: return self.no_streams_found()

    if   len(self.streams['hd']):       streams += self.streams['hd']
    elif len(self.streams['veryhigh']): streams += self.streams['veryhigh']
    elif len(self.streams['high']):     streams += self.streams['high']
    elif len(self.streams['med']):      streams += self.streams['med']
    elif len(self.streams['low']):      streams += self.streams['low']
    else: return self.no_streams_found()

    streams.sort(key=lambda x: int(x['vbr']), reverse=True)

    return 0

  def get_video_filename(self, zeit, title):
    title = re.sub( ' vom [0-9]{1,2}\. [A-Z][a-zä]* [0-9]{4}$', '', title )
    return super().get_video_filename(zeit, title)

  """ invoke that only after get_video_streams_for_download ! """
  def get_video_filename_for_download(self):
    self.parse_xml()
    return self.get_video_filename(self.airtime, self.title)

  def test_can_download( self, stream ):
    if "metafilegenerator" in stream['url']:
      return False # download forbidden
    if re.search('.mov$', stream['url']):
      return False # did not yet find out how to download rtsp files
    return True
