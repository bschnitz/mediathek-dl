import re
import datetime
import configparser

import xml.etree.ElementTree as ET

from .broadcast import Broadcast
from mediathek_dl.tools import TermiTable

class Mediathek():
  def __init__(self):
    self.path_broadcast_db = "broadcasts.ini"
    self.broadcast_db = configparser.ConfigParser(interpolation=None)
    self.have_parser = False
    self.have_broadcast_list = False
    self.broadcasts = []

    self.body_cols = [ 'mediathek', 'hash', 'zeit', 'thema', 'titel', 'dauer' ]
    self.head_cols = [ "Quelle", "Id", "Zeit", "Thema",  "Titel", "Dauer" ]
    self.resizable_cols = [ 3, 4 ]
    self.align_head = ["-"] * 6
    self.align_body = ["-"] * 6
    self.default_search_in = ['titel', 'thema']
    self.default_sort_after = ['zeit', 'thema', 'titel']

  def has_video_id(self, Id):
    if not self.have_broadcast_list: self.read_broadcasts_from_database()

    if 'keymap' in self.broadcast_db.sections():
      return Id in self.broadcast_db['keymap']
    else: return Id in self.broadcast_db

  def init_parser(self):
    self.parser = ET.XMLParser()
    self.parser.entity['#150'] = '-'
    self.parser.entity['nbsp'] = chr(0x24B8)
    self.parser.entity['auml'] = chr(0xE4)
    self.parser.entity['Ouml'] = chr(0xD6)
    self.parser.entity['uuml'] = chr(0xFC)
    self.parser.entity['ouml'] = chr(0xF6)
    self.parser.entity['Auml'] = chr(0xC4)
    self.parser.entity['Uuml'] = chr(0xDC)
    self.parser.entity['szlig'] = chr(0xDF)
    self.parser.entity['oacute'] = chr(0xF3)
    self.parser.entity['ugrave'] = chr(0xD9)
    self.parser.entity['ugrave'] = chr(0xD9)
    self.parser.entity['egrave'] = chr(0xE8)
    self.parser.entity['aacute'] = chr(0xE1)
    self.parser.entity['eacute'] = chr(0xE9)
    self.parser.entity['ndash'] = chr(0x2013)
    self.parser.entity['bdquo'] = chr(0x201E)
    self.parser.entity['ldquo'] = chr(0x201C)
    self.parser.entity['yuml'] = chr(0xFF)
    self.parser.entity['acute'] = chr(0xB4)
    

  def get_date(self, days_diff = 0):
    dd = datetime.date
    return dd.fromordinal( dd.today().toordinal() + days_diff )

  def search_broadcasts(self, pattern, search_in_columns, addtothese=None):
    if search_in_columns == None: search_in_columns = self.default_search_in

    if not self.have_broadcast_list: self.read_broadcasts_from_database()

    if addtothese != None: result = addtothese
    else:                  result = []

    if pattern == None: result += self.broadcasts
    else:
      for broadcast in self.broadcasts:
        for colname in search_in_columns:
          value = broadcast.get_col( colname )
          if value and re.search(pattern, value, re.IGNORECASE):
            result.append(broadcast)
            break

    return result

  def test_info_cols(self, columns, error):
    error += "\nMögliche Spalten sind: " + ','.join( self.info_cols )
    if columns != None:
      for col in columns:
        if not col in self.info_cols:
          print( error % col )
          return False
    return True

  def read_broadcasts_from_database(self):
    self.broadcast_db.read(self.path_broadcast_db)

    for section in self.broadcast_db.sections():
      bc = self.read_broadcast_from_database(section)
      if bc != None: self.broadcasts.append( bc )
    self.have_broadcast_list = True

  def read_broadcast_from_database(self, section):
    return Broadcast(self.broadcast_db, section)

  def print_broadcasts(self, broadcasts, sort_after):
    if sort_after == None: sort_after = self.default_sort_after
    Broadcast.set_compare_after_colnames( sort_after )
    broadcasts.sort()

    tt = TermiTable( len(self.body_cols), "  ")
    tt.set_resizable( self.resizable_cols )
    tt.set_align_head( self.align_head )
    tt.set_alignment( self.align_body )

    tt.add_row( self.head_cols )

    tt.append_broadcasts( broadcasts, self.body_cols )

    tt.recalculate_column_sizes()
    tt.print_table()
