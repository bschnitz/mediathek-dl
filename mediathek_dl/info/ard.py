import os
import urllib.error
import urllib.request

from lxml import html

class ARDInfo():
  def __init__(self, broadcast):
    self.set_broadcast(broadcast)
    self.html_output_file = "files/ard/" + str(self.video_id) + ".html"

  def set_broadcast(self, broadcast):
    self.baseaddress = "http://www.ardmediathek.de"
    self.video_id = broadcast.get_col('id')
    self.broadcast = broadcast

    self.image      = self.baseaddress + broadcast.get_col('bild')
    self.videourl   = self.baseaddress + broadcast.get_col('link')
    self.streaminfo = self.baseaddress + '/play/media/' + str(self.video_id)

  def get_html_data(self, url):
    if not os.path.isfile( self.html_output_file ):
      try:
        ures = urllib.request.urlopen( url ).read().decode('utf8')
        with open( self.html_output_file, 'w' ) as fp: fp.write(ures)
      except urllib.error.HTTPError:
        print("Fehler beim öffnen von:\n " + url)
        print("Videoinformationen konnten nicht geladen werden.")
        ures = None
    else:
      with open( self.html_output_file, 'r' ) as fp: ures = fp.read()

    return ures

  def get_video_info(self):
    video_info = {}

    video_info['id'] = self.broadcast.get_col('id')
    video_info['titel'] = self.broadcast.get_col('titel')
    video_info['thema'] = self.broadcast.get_col('thema')
    video_info['sender'] = self.broadcast.get_col('kanal')
    video_info['sendezeit'] = self.broadcast.get_col('zeit')
    video_info['spieldauer'] = self.broadcast.get_col('dauer')
    video_info['links'] = []

    html_root = html.fromstring( self.get_html_data(self.videourl) )
    html_root = html_root.find_class('modClipinfo')[0]
    html_root = html_root.xpath('.//div[@class="teaser"]')[0]

    inhalt = html_root.find_class('teasertext')
    if len(inhalt) > 0: video_info['inhalt'] = inhalt[0].text_content()
    else:               video_info['inhalt'] = None

    links = html_root.xpath('./div[@class="button"]/a')
    if len(links) > 0:
      link = self.baseaddress + links[0].get('href')
      video_info['links'].append(['Sendungsseite', link])

    video_info['info'] = html_root.find_class('subtitle')[0].text_content()

    return video_info

  def print_video_info(self):
    try:
      video_info = self.get_video_info()
    except Exception as e:
      videoid = self.broadcast.get_col('id')
      print( "Fehler beim Laden der Videodaten zu: " + videoid )
      print( 'Link zur Seite in der Ard Mediathek:\n  ' + self.videourl )
      raise e

    print('%s\n' % video_info['titel'])
    print( '%-15s %s' % ('Id: ',              video_info['id'])         )
    if 'thema' in video_info:
      print( '%-15s %s\n' % ('Aus der Reihe: ', video_info['thema'])      )
    print( video_info['info'] + '\n' )

    if video_info['inhalt']:
      print( 'Inhaltsangabe:\n%s\n' % video_info['inhalt'] )

    print( 'Link zur Seite in der Ard Mediathek:\n  ' + self.videourl )
    for link in video_info['links']:
      print('%s:\n  %s' % (link[0], link[1]) )
    print( 'Bild:\n  ' + self.image + '640' )
    print( 'Link zur JSON-Streamdatei:\n  ' + self.streaminfo )
