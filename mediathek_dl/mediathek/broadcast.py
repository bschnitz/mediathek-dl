class Broadcast():
  compare_after_colnames = None

  @staticmethod
  def try_add_new_broadcast( broadcast_database, id ):
    if id in broadcast_database: return None
    else: return Broadcast( broadcast_database, id )

  def __init__( self, broadcast_database, id ):
    self.id = id
    self.db = broadcast_database
    if not id in broadcast_database:
      broadcast_database[self.id] = {}

  def set_compare_after_colnames( list_of_colnames ):
    Broadcast.compare_after_colnames = list_of_colnames

  def add_col(self, colname, content): self.db[self.id][colname] = content

  def get_col(self, name):
    if name == 'id': return self.id
    if not name in self.db[self.id]: return None
    return self.db[self.id][name]

  def have_col(self, name): return name in self.db[self.id]

  def add_col_if_not_def(self, colname, content):
    if not self.have_col(colname): self.add_col(colname, content)

  def __lt__(self, other):
    assert Broadcast.compare_after_colnames
    for colname in Broadcast.compare_after_colnames:
      self_value  = self.get_col(colname)
      other_value = other.get_col(colname)

      if    not self_value and other_value: return True
      elif  not other_value and self_value: return False
      elif  not other_value and not self_value: continue

      if colname == 'dauer':
        self_value = '%10s' % self_value
        other_value = '%10s' % other_value

      if self_value < other_value: return True
      if self_value > other_value: return False
    return False

  def to_list( self, colnames ):
    result = []
    for name in colnames:
      value = self.get_col( name )
      if not value: value = ""
      result.append(value)
    return result
