from .base import MediathekDownload
from mediathek_dl.mediathek import ARDMediathek

import os
import re
import json
import urllib.error
import urllib.request

class ARDDownload(MediathekDownload):
  def __init__(self, video_id):
    self.channel = 'ard'
    self.path_broadcast_db = 'ard.ini'
    super().__init__( video_id )

  def set_video_id( self, video_id ):
    self.video_id = video_id

    self.baseaddress = "http://www.ardmediathek.de"
    self.streaminfo = self.baseaddress + '/play/media/' + str(video_id)
    self.html_output_file = "files/ard/" + str(video_id) + ".html"
    self.json_output_file = "files/ard/" + str(video_id) + ".json"
    self.img_output_file  = "files/ard/" + str(video_id) + ".jpg"

  def save_video_data(self):
    self.open_broadcast_db()
    self.image_url = self.baseaddress + self.broadcast_db[self.video_id]['bild']
    self.image_url += '960'
    html_url = self.baseaddress + self.broadcast_db[self.video_id]['link']
    html_dst = self.dl_folder + self.html_output_file
    urllib.request.urlretrieve( html_url, html_dst )
    self.files = [ self.json_output_file ]
    super().save_video_data()

  def download_json(self):
    if not re.match( "[0-9]*$", self.video_id ):
      print( "Ungültige Video Id (darf nur aus Zahlen bestehen)." )
      return None

    response = urllib.request.urlopen( self.streaminfo )

    try:
      json_data = json.loads( response.read().decode('utf-8') )

      os.makedirs("files/ard", exist_ok=True)
      with open(self.json_output_file, "w") as json_file:
        json_string = json.dumps(json_data,indent=4,separators=(',', ': '))
        json_file.write(json_string)
    except ValueError as e:
      print( 
        "Fehler beim Laden der json-Datei, möglicherweise ist die Id ungültig."
      )
      return None

    return json_data

  def get_video_streams(self):
    json = self.download_json()
    video_streams = {}
    if json:
      for entry in json['_mediaArray']:
        for sentry in entry['_mediaStreamArray']:
          quality = sentry['_quality']
          if quality != 'auto':
            if not quality in video_streams: video_streams[quality] = []
            server = sentry['_server'] if '_server' in sentry else ""
            server = server.rstrip('/') + '/' if server != "" else ""
            if type(sentry['_stream']) is list:
              stream = sentry['_stream'][-1]
            else: stream = sentry['_stream']
            stream = server + re.sub( 'mp4:', '', stream )
            video_streams[quality].append(stream)

    if json and len(video_streams) == 0:
      print("Keine Video Streams gefunden!")
      print("Wird das Video erst später freigeschaltet (wegen FSK)?")
      print("Ist die Sendung noch in der Mediathek vorhanden und abspielbar?")

    return video_streams

  def print_video_streams(self):
    streams = self.get_video_streams()
    if len(streams) > 0:
      print("Je höher der Qualitätsindex, desto besser die Qualität.\n")
      for key in sorted(streams.keys()):
        print( "Qualitätsindex " + str(key) )
        for stream in streams[key]:
          print( "  " + stream )

  def get_video_filename(self):
    mediathek = ARDMediathek()
    broadcast = mediathek.search_broadcasts(self.video_id, ['id'])[0]
    date = broadcast.get_col('zeit')[:10]
    title = broadcast.get_col('titel')
    title = re.sub(' \(Video tgl. ab 20 Uhr\)', '', title)
    title = self.clean_video_filename(title)
    return date + '--' + title

  """ invoke that only after get_video_streams_for_download ! """
  def get_video_filename_for_download(self): return self.get_video_filename()

  def get_video_streams_for_download(self, streams):
    vstreams = self.get_video_streams()
    for key in reversed(sorted(vstreams.keys())):
        for url in vstreams[key]:
          streams.append( {'url' : url} )
    return 0

  def test_can_download( self, stream ):
    assert re.search('.*\.mp4', stream['url'] )
    if re.search( '.*\.f4m$', stream['url'] ):
      return False # don't know how to download
    return True
