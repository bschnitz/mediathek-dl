#!/usr/bin/env python3

import os
import sys
import locale
import argparse

from mediathek_dl.mediathek import Config
from mediathek_dl.mediathek import Mediathek
from mediathek_dl.mediathek import ARDMediathek, ZDFMediathek, ARTEMediathek
from mediathek_dl.mediathek import RecMediathek
from mediathek_dl.mediathek.other import get_mediathek_dl

from mediathek_dl.download import ARDDownload, ZDFDownload, ARTEDownload

from mediathek_dl.merge import Merge

from mediathek_dl.recorder import Recorder

from mediathek_dl.tools import logme

def main():
  locale.setlocale(locale.LC_ALL, 'de_DE.UTF-8')
  rval = Mediatheken(sys.argv).call()
  if rval != None: sys.exit(rval)

class Mediatheken():
  def __init__(self, argv):
    self.parsers = ParseArgs(argv)

    self.parsers.add_help_parser(self.parsers.print_help)
    self.parsers.add_update_parser(self.update)
    self.parsers.add_show_parser(self.show)
    self.parsers.add_info_parser(self.info)
    self.parsers.add_stream_parser(self.streams)
    self.parsers.add_merge_parser(self.merge)
    self.parsers.add_download_parser(self.download)
    self.parsers.add_init_parser(self.init)
    self.parsers.add_record_parser(self.record)
    self.parsers.add_startrec_parser(self.startrec)
    self.parsers.add_clearrec_parser(self.clearrec)
    self.parsers.add_test_parser(self.test)

  def call(self):
    self.parsers.call()

  def test_is_initialized(func):
    def wrapper( self, parsers ):
      if Config().check(): func( self, parsers )
      else: print( "Dies ist kein mediathek-dl Verzeichnis (s. help init)." )
    return wrapper

  def create_download_directories(self):
    os.makedirs('downloads/videos/laufend', exist_ok=True)
    os.makedirs('downloads/files/arte', exist_ok=True)
    os.makedirs('downloads/files/ard', exist_ok=True)
    os.makedirs('downloads/files/zdf', exist_ok=True)

  def get_mediathek_dl(self, sendung_id, channel=None):
    if not channel: channel = self.parsers.args.channel
    return get_mediathek_dl( sendung_id, channel )

  def get_mediathek(self, channel=None):
    if not channel: channel = self.parsers.args.channel

    if channel == 'zdf':  return [ ZDFMediathek()  ]
    if channel == 'ard':  return [ ARDMediathek()  ]
    if channel == 'arte': return [ ARTEMediathek() ]
    if channel == 'all': return [ZDFMediathek(),ARDMediathek(),ARTEMediathek()]
    if channel == 'rec': return [ RecMediathek() ]

    return None

  @test_is_initialized
  def streams(self, parsers):
    dl = self.get_mediathek_dl( str(parsers.args.id[0]) )
    dl.print_video_streams()

  @test_is_initialized
  def download(self, parsers):
    self.create_download_directories()
    dl = self.get_mediathek_dl( parsers.args.id[0] )
    rval = dl.downloader( parsers.args )
    logme( "Rückgabewert " + parsers.args.id[0] + ": " + str(rval), 2 )
    return rval

  @test_is_initialized
  def record(self, parsers):
    self.create_download_directories()
    dl = self.get_mediathek_dl( parsers.args.id[0] )
    dl.recorder( parsers.args )

  @test_is_initialized
  def startrec(self, parsers):
    self.create_download_directories()
    rec = Recorder( parsers.args )
    rec.start()

  def clearrec(self, parsers):
    rec = Recorder( parsers.args )
    rec.clearrec()

  def show(self, parsers):
    args = parsers.args

    if args.search_in  != None: args.search_in  = args.search_in[0].split(",")
    if args.sort_after != None: args.sort_after = args.sort_after[0].split(",")

    mediatheken = self.get_mediathek()

    broadcasts = []
    for mediathek in mediatheken:
      error = "Fehler: falsche Spaltenangabe '%s' für --search-in"
      if not mediathek.test_info_cols( args.search_in, error ): sys.exit(0)

      error = "Fehler: falsche Spaltenangabe '%s' für --sort-after"
      if not mediathek.test_info_cols( args.sort_after, error ): sys.exit(0)

      mediathek.search_broadcasts( args.searchstr, args.search_in, broadcasts )

    if_not_found = "Keine Sendungen zu den angegebenen Suchkriterien gefunden."
    if not len(broadcasts): print(if_not_found)
    elif len(mediatheken) == 1:
      mediatheken[0].print_broadcasts( broadcasts, args.sort_after )
    else:
      Mediathek().print_broadcasts( broadcasts, args.sort_after )

  def info(self, parsers):
    args = parsers.args
    channel = parsers.args.channel

    mediathek = self.get_mediathek()[0]

    if args.search_in  == None:
      if mediathek.has_video_id(args.searchstr[0]):
        if channel == 'arte': args.search_in  = ['hash']
        else:                 args.search_in  = ['id']
      else:                                 args.search_in = ['titel,thema']

    search_in  = args.search_in[0].split(",")

    error = "Fehler: falsche Spaltenangabe '%s' für --search-in"
    if not mediathek.test_info_cols( search_in, error ): sys.exit(0)

    broadcasts = mediathek.search_broadcasts(args.searchstr[0], search_in)
    if not len(broadcasts):
      print("Keine Sendungen zu den angegebenen Suchkriterien gefunden.")
      sys.exit(1)

    mediathek.print_broadcasts_info(broadcasts)

  def init(self, parsers): Config().init()

  @test_is_initialized
  def test(self, parsers):
    config = Config().read()
    if config.check_flag('use_nrodl'):
      print("Using nrodl")
    else:
      print("not using nrodl")
    print(config['use_nrodl'])

  @test_is_initialized
  def update(self, parsers):
    mediathek = self.get_mediathek()[0]
    mediathek.update_broadcast_database()

  @test_is_initialized
  def merge(self, parsers):
    Merge(parsers.args)

class ParseArgs():
  def __init__(self, argv):
    self.argv    = argv
    self.parsers = {}
    self.subcmd  = None
    self.parsers['main'] = argparse.ArgumentParser()

    # add a subcommand and a parser for all available commands
    self.add_subcmd()

  def add_subcmd(self):
    descr = sys.argv[0] + " help 'Befehl' eingeben um eine erweiterte Hilfe" \
                        + " für die nachfolgenden Befehle zu erhalten."
    main_parser = self.parsers['main']
    self.subcmd = main_parser.add_subparsers( description=descr )
    return self.subcmd

  def add_arg_channel(self, parser, canall=False):
    Help = 'Sender wählen (Default: zdf)'
    channels = ['ard', 'zdf', 'arte', 'rec']
    if canall: channels.append('all')
    parser.add_argument( '--channel', '-c',
                         choices=channels, default='zdf', help=Help )

  def add_help_parser(self, func):
    Help = 'Hilfe zu den verfügbaren Befehlen anzeigen'
    parser = self.subcmd.add_parser( 'help', help=Help )

    Help = "Hilfe zum Befehl 'CMD' anzeigen"
    parser.add_argument( 'cmd', default='main',
                         type=str, metavar='CMD', nargs='?', help=Help )

    parser.set_defaults( func=func )

    self.parsers['help'] = parser
    return parser

  def add_update_parser(self, func):
    Help = 'Liste der Sendungen aktualisieren.'
    parser = self.subcmd.add_parser( 'update', help=Help, description=Help )
    parser.set_defaults( func=func )
    self.parsers['update'] = parser
    self.add_arg_channel(parser)
    return parser

  def add_show_parser(self, func):
    Help   = 'Verfügbare Sendung suchen/anzeigen.'
    parser = self.subcmd.add_parser( 'show', help=Help, description=Help )

    self.add_arg_channel(parser, canall=True)

    ident = 'searchstr'
    Help  = 'optionaler Suchstring'
    mvar  = 'STRING'
    parser.add_argument( ident, type=str, metavar=mvar, nargs='?', help=Help )

    option = '--sort-after'
    Help   =   'Sortierung der Spalten in angegebener Reihenfolge. '\
             + 'Mögliche Angaben sind: id,zeit,thema,dauer,titel . '\
             + 'Default Wert: zeit,thema,titel'
    mvar   = 'C1,C2,...'
    parser.add_argument( option, '-s', nargs=1, metavar=mvar, help=Help )

    option = '--search-in'
    Help   =   'Suche in den Einträgen der angegebenen Spalten durchführen. '\
             + 'Mögliche Angaben sind: id,zeit,thema,dauer,titel . '\
             + 'Default Wert: titel,thema'
    mvar   = 'C1,C2,...'
    parser.add_argument( option, '-i', nargs=1, metavar=mvar, help=Help )

    parser.set_defaults(func=func)

    self.parsers['show'] = parser
    return parser

  def add_info_parser(self, func):
    Help   = 'Nähere Informationen zu einer Sendung anzeigen.'
    parser = self.subcmd.add_parser( 'info', help=Help, description=Help )

    self.add_arg_channel(parser)

    ident = 'searchstr'
    Help  = 'Suchstring oder Id: Informationen zu Sendung mit Id ID anzeigen, ' \
          + 'oder alternativ Informationen zu Sendungen anzeigen lassen, die ' \
          + 'durch den Suchstring "STRING" ermittelt werden können.'
    mvar  = 'ID/STRING'
    parser.add_argument( ident, type=str, metavar=mvar, nargs=1, help=Help )

    option = '--search-in'
    Help   =   'Suche in den Einträgen der angegebenen Bereiche durchführen. '\
             + 'Mögliche Angaben sind: id,zeit,thema,dauer,titel . '\
             + 'Default Wert: titel,thema (bzw. id, wenn reine Zahl gegeben)'
    mvar   = 'C1,C2,...'
    parser.add_argument( option, '-i', nargs=1, metavar=mvar, help=Help )

    parser.set_defaults( func=func )

    self.parsers['info'] = parser
    return parser

  def add_stream_parser(self, func):
    Help = 'Verfügbare Streams zu einer Sendung anzeigen'
    parser = self.subcmd.add_parser( 'streams', help=Help, description=Help )

    self.add_arg_channel(parser)

    Help = 'Streams der Sendung mit Id "ID" anzeigen'
    parser.add_argument( 'id', metavar='ID', nargs=1, help=Help )

    parser.set_defaults(func=func)

    self.parsers['streams'] = parser
    return parser

  def add_merge_parser(self, func):
    Help = 'Diese Mediathek-dl Verzeichnisstruktur in eine andere einpflegen.'
    epilog = 'Das Zielverzeichnis muss wie das aktuelle Verzeichnis ein ' \
             'Mediathek Download Verzeichnis sein. Enthält eine der ' \
             'Videodatenbanken (.ini-Dateien) im download Verzeichnis der' \
             'Zielverzeichnisstruktur eine Videoid, die auch in den ' \
             'des Downloadverzeichnisses der aktuellen Verzeichnisstruktur' \
             'enthalten ist, so wird keine Zusammenführung durchgeführt, ' \
             'stattdessen werden die Überschneidungen ausgegeben ' \
             '(Ausnahmen: s. -f, -i und -o Schalter).'
    parser = self.subcmd.add_parser(
        'merge', help=Help, description=Help, epilog=epilog
    )

    Help = 'Zielverzeichnis (anderes Mediathek-dl Verzeichnis)'
    parser.add_argument( 'dest', metavar='DESTDIR', help=Help )

    option = '--force'
    Help   = 'Merge erzwingen. Überschreibt bereits vorhandene Videos im ' \
             'Zielverzeichnis.'
    parser.add_argument( option, '-f', help=Help, action='store_true' )

    # TODO: implement that; see also the todo in merge.py
    #option = '--overwrite'
    #Help   = 'Überschreibt die mediathek-dl.ini Konfigurationsdateien im ' \
    #         'Zielverzeichnis. Falls -f nicht gegeben ist und einige Videos ' \
    #         'bereits in der Zielverzeichnisstruktur vorhanden sind, so wird ' \
    #         'nur -o ausgeführt.'
    #parser.add_argument( option, '-o', help=Help, action='store_true' )

    option = '--ignore-existing'
    Help   = 'Falls es Videoid Überschneidungen gibt, für die die Videotitel' \
             'übereinstimmen, so werden diese Überschneidungen ignoriert.'
    parser.add_argument( option, '-i', help=Help, action='store_true' )

    parser.set_defaults(func=func)

    self.parsers['merge'] = parser
    return parser

  def add_init_parser(self, func):
    Help = 'Aktuelles Verzeichnis als Verzeichnis' + \
           'für den Mediathek Download einrichten.'
    parser = self.subcmd.add_parser( 'init', help=Help, description=Help )

    parser.set_defaults(func=func)

    self.parsers['init'] = parser

    return parser

  def add_test_parser(self, func):
    parser = self.subcmd.add_parser( 'test' )

    parser.set_defaults(func=func)

    self.parsers['test'] = parser

    return parser

  def add_download_parser(self, func):
    ident = 'download'
    Help  = 'Sendung herunterladen (bzw. streamen)'
    Alt   = ['dl']
    parser = self.subcmd.add_parser( ident, help=Help,
                                     description=Help, aliases=Alt )

    self.add_arg_channel(parser)

    Help = 'Sendung mit Id "ID" herunterladen'
    parser.add_argument( 'id', type=str, metavar='ID', nargs=1, help=Help )

    option = '--restart'
    Help   = 'Download neustarten (auch wenn schon abgeschlossen).'
    parser.add_argument( option, '-r', help=Help, action='store_true' )

    option   = '--trickle'
    Help = 'Download Geschwindigkeit Begrenzen auf KB Kilobyte/s'
    parser.add_argument(option,'-t', help=Help, nargs=1, metavar='KB', type=int)

    option   = '--records-cfg'
    Help = 'Download einer in FILE vorgemerkten Aufnahme (s. help record).'
    parser.add_argument(option,'-d',help=Help, nargs=1,metavar='FILE', type=str)

    parser.set_defaults(func=func)

    self.parsers[ident] = parser
    return parser

  def add_record_parser(self, func):
    ident = 'record'
    Help  = 'Sendungsdaten für die automatische Aufnahme speichern.'
    Alt   = ['rec']
    parser = self.subcmd.add_parser( ident, help=Help,
                                     description=Help, aliases=Alt )

    self.add_arg_channel(parser)

    Help = 'Sendung mit Id "ID" aufnehmen.'
    parser.add_argument( 'id', type=str, metavar='ID', nargs=1, help=Help )

    option = '--restart'
    Help   = 'Falls Download schon begonnen wurde, diesen neustarten.'
    parser.add_argument( option, '-r', help=Help, action='store_true' )

    option   = '--trickle'
    Help = 'Download Geschwindigkeit Begrenzen auf KB Kilobyte/s'
    parser.add_argument(option,'-t', help=Help, nargs=1, metavar='KB', type=int)

    parser.set_defaults(func=func)

    self.parsers[ident] = parser
    return parser

  def add_clearrec_parser(self, func):
    ident = 'clearrec'
    Help  = ('Datenbank für die automatische Aufnahme von abgeschlossenen '
             'Aufnahmen bereinigen')
    parser = self.subcmd.add_parser( ident, help=Help, description=Help )

    parser.set_defaults(func=func)

    self.parsers[ident] = parser
    return parser

  def add_startrec_parser(self, func):
    ident = 'startrec'
    Help  = 'Automatische Aufnahme der Sendungen beginnen.'
    parser = self.subcmd.add_parser( ident, help=Help, description=Help )

    parser.set_defaults(func=func)

    self.parsers[ident] = parser
    return parser

  def call(self):
    self.args = self.parsers['main'].parse_args(self.argv[1:])
    if 'func' in self.args:
      return self.args.func(self)
    else:
      self.args.cmd = 'main'
      self.print_help()

  def print_help(self, *args):
    if self.args.cmd not in self.parsers:
      print( "Fehler: unbekannter Befehl: '" + self.args.cmd + "'" )
      print( "'" + self.argv[0] + " help' " + "liefert eine Übersicht")
    else: self.parsers[self.args.cmd].print_help()

if __name__ == "__main__":
  main()
