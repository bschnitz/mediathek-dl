# prefer rtmp streams for download (set to False if preferring wget)
from .base import MediathekDownload

import os
import re
import json
import urllib.request
from mediathek_dl.tools import TermiTable
from mediathek_dl.mediathek.config import Config

from lxml import html, etree

class ArteFskException(Exception): pass

class ARTEDownload(MediathekDownload):
  def __init__(self, video_hash):
    self.channel = 'arte'
    self.base_addr = 'http://www.arte.tv/guide/de'
    self.path_broadcast_db = 'arte.ini'
    super().__init__( video_hash )

  def get_video_id(self, video_hash):
    self.open_broadcast_db()
    video_ids = self.broadcast_db['keymap'][video_hash].split(',')
    assert( len(video_ids) == 1 )
    return video_ids[0]

  def get_video_id_as_filename(self, video_id):
    return re.sub( '/', '_', self.video_id )

  def get_json_output_file( self, video_id ):
    id_as_filename = self.get_video_id_as_filename(video_id)
    return "files/arte/" + id_as_filename + ".json"

  def set_json_output_file( self, video_id ):
    self.json_output_file = self.get_json_output_file(video_id)

  def set_html_output_file( self, video_id ):
    id_as_filename = self.get_video_id_as_filename(video_id)
    self.html_output_file = "files/arte/" + id_as_filename + ".html"

  def set_img_output_file( self, video_id ):
    id_as_filename = self.get_video_id_as_filename(video_id)
    self.img_output_file  = "files/arte/" + id_as_filename + ".jpg"

  def set_video_id(self, video_hash):
    self.have_bc_db = False
    self.video_id = self.get_video_id(video_hash)
    self.video_hash = video_hash
    self.html_is_parsed = False
    self.json_is_parsed = False
    self.json_is_loaded = False
    self.video_info = self.get_video_info(self.video_id)
    self.video_streams = None
    self.set_json_output_file(self.video_id)
    self.set_html_output_file(self.video_id)
    self.set_img_output_file(self.video_id)

  def parse_zeit_genau(self, zeit_genau):
    pattern = "^(..).(..).(....) (..:..).*$"
    return re.sub( pattern, '\g<3>-\g<2>-\g<1> \g<4>', zeit_genau )

  def get_video_info(self, video_id):
    self.open_broadcast_db()

    video_info             = {}
    video_info['key']      = video_id
    video_info['hash']     = self.broadcast_db[video_id]['hash']
    video_info['url']      = self.base_addr + "/" + video_id
    video_info['titel']    = self.broadcast_db[video_id]['titel']
    video_info['dauer']    = self.broadcast_db[video_id]['dauer']
    video_info['kurztext'] = self.broadcast_db[video_id]['kurztext']
    video_info['bild']     = self.broadcast_db[video_id]['bild']
    video_info['adult']    = self.broadcast_db[video_id]['adult']

    self.video_info = video_info

    self.set_html_output_file(video_id)
    self.set_json_output_file(video_id)
    self.parse_json()

    if 'thema' in self.broadcast_db[video_id]:
      video_info['thema']    = self.broadcast_db[video_id]['thema']
    elif 'art' in self.video_info:
      video_info['thema'] = self.video_info['art']

    video_info['zeit'] = self.parse_zeit_genau(self.video_info['zeit_genau'])

    if 'untertitel' in self.broadcast_db[video_id]:
      video_info['untertitel']    = self.broadcast_db[video_id]['untertitel']

    return video_info

  def save_video_data(self):
    self.open_broadcast_db()
    self.image_url = self.broadcast_db[self.video_id]['bild']
    self.broadcast_db[self.video_id]['zeit'] = self.video_info['zeit']
    self.broadcast_db[self.video_id]['thema'] = self.video_info['thema']
    self.files = [ self.html_output_file, self.json_output_file ]
    super().save_video_data()

  def download_html( self, url, dest ):
    print("Downloading Video HTML file.")
    urllib.request.urlretrieve( url, dest )
    print("Download of HTML file is complete.")

  def get_short_description(self, html_root):
    xpath = '//section[@id="details-description"]//p[@class="description"]'
    tag = html_root.xpath(xpath)[0]
    match = re.search('\S.*\S', tag.text_content(), re.MULTILINE | re.DOTALL)
    return match.group(0)

  def get_long_description(self, html_data):
    html_root = html.fromstring(html_data)
    try:
      tag = html_root.xpath('//div[@data-action="description"]')[0]
      match = re.search('\S.*\S', tag.text_content(), re.MULTILINE | re.DOTALL)
      if match:
        return match.group(0)
      else:
        tag = html_root.xpath('//div[contains(@class, "description_short")]')[0]
        match = re.search('\S.*\S',tag.text_content(), re.MULTILINE | re.DOTALL)
        return match.group(0)
    except IndexError:
      return self.get_short_description(html_root)

  def get_json_addr(self, html_data):
    match = re.search("'([^']*ALL.json)'", html_data, re.MULTILINE | re.DOTALL)

    if match: return match.group(1)

    html_root = html.fromstring(html_data)
    tag = html_root.xpath('//p[@class="time-row"]')[0]
    span = tag.xpath('./span')[0]
    span.text = ""
    os.remove(self.html_output_file)
    raise ArteFskException(tag.text_content().strip())

  def parse_html(self):
    if not os.path.exists( self.html_output_file ):
      self.download_html( self.video_info['url'], self.html_output_file )

    flags = re.MULTILINE | re.DOTALL
    re_description = '<section[^>]*action="description"[^>]*>(.*?)</section>'
    with open(self.html_output_file, "r") as html_data:
      html_source = html_data.read()
      self.video_info['inhalt'] = self.get_long_description(html_source)
      self.video_info['json']   = self.get_json_addr(html_source)

    self.html_is_parsed = True

  def download_json(self):
    if not self.html_is_parsed: self.parse_html()

    response = urllib.request.urlopen( self.video_info['json'] )

    self.json_data = json.loads( response.read().decode('utf-8') )

    with open(self.json_output_file, "w") as json_file:
      json_string = json.dumps(self.json_data, indent=4, separators=(',', ': '))
      json_file.write(json_string)

    self.json_is_loaded = True

  def load_json(self):
    if not os.path.exists( self.json_output_file ):
      self.download_json()
      return

    with open(self.json_output_file) as json_file:
      self.json_data = json.loads(json_file.read())

    self.json_is_loaded = True

  def json_get(self, *args):
    if not self.json_is_loaded: self.load_json()

    json_data = self.json_data['videoJsonPlayer']
    for arg in args:
      if arg in json_data: json_data = json_data[arg]
      else:                return None
    return json_data

  def parse_json(self):
    if not self.json_is_loaded: self.load_json()

    key_sec = 'videoDurationSeconds'
    self.video_info['titel_zusatz']      = self.json_get('VSU')
    self.video_info['art']               = self.json_get('VCG')
    self.video_info['dauer_in_sekunden'] = self.json_get('videoDurationSeconds')
    self.video_info['zeit_genau']        = self.json_get('VRA')

    keywords = set()
    for key in [ "VCH", "VCM" ]:
      for entry in self.json_get(key):
        keywords = keywords | set(re.split(' / | & ', entry['label']))

    self.video_info['keywords'] = ','.join(keywords)

    self.json_is_parsed = True
    return True

  def print_json(self, json_data, *args):
    print(args)
    for arg in args:
      if arg in json_data: json_data = json_data[arg]
      else:                return
    print( json.dumps(json_data, indent=4, separators=(',', ': ')) )

  def debug_examine_json(self):
    if not self.json_is_loaded: self.load_json()

    #self.print_json(self.json_data['videoSearchParams'])
    #self.print_json(self.json_data['videoJsonPlayer'])
    self.print_json(self.json_data, 'videoJsonPlayer', 'VSR')
    print()
    self.print_json(self.json_data, 'videoJsonPlayer', 'VTU', 'IUR')
    print()
    self.print_json(self.json_data, 'videoJsonPlayer', 'V7T')
    print()
    self.print_json(self.json_data, 'videoJsonPlayer', 'programImage')
    print()
    self.print_json(self.json_data, 'videoJsonPlayer', 'VCG')
    print()
    self.print_json(self.json_data, 'videoJsonPlayer', 'videoDurationSeconds')
    print()
    self.print_json(self.json_data, 'videoJsonPlayer', 'VSU')
    print()
    self.print_json(self.json_data, 'videoJsonPlayer', 'VRU')
    print()
    self.print_json(self.json_data, 'videoJsonPlayer', 'VRA')

  def get_video_streams(self):
    self.download_json()

    self.video_info['streams'] = []
    streams = self.json_get('VSR')
    for quality in streams:
      stream = streams[quality]
      s_info = {}

      s_info['url']     = stream['url']
      if 'streamer' in stream:
        s_info['rtmp']  = stream['streamer']
        s_info['url'] = 'mp4:' + s_info['url']

      s_info['breite']  = stream.get("width", 0)
      s_info['hoehe']   = stream.get("height", 0)
      s_info['bitrate'] = stream['bitrate']
      s_info['sprache'] = stream['versionShortLibelle']


      self.video_info['streams'].append(s_info)

    def stream_sort_code(stream):
      lang_map = { 'DE':4, 'OmU':3, 'FR':2, 'HÖR':1, 'UTH':0 }
      lang_idx = lang_map[stream['sprache']]
      br = int(stream['bitrate'])
      width = int(stream['breite'])
      kind = ('rtmp' in stream) == Config().check_flag('arte', 'prefer_rtmp')
      return "%d%4d%4d%d" % (lang_idx, br, width, kind)
    self.video_info['streams'].sort(key=stream_sort_code)

    self.have_video_streams = True

  def print_video_streams(self):
    if not 'streams' in self.video_info: self.get_video_streams()

    for stream in self.video_info['streams']:
      print()

      if   stream['sprache'] == 'DE':  language = "Deutsch"
      elif stream['sprache'] == 'FR':  language = "Französisch"
      elif stream['sprache'] == 'HÖR': language = "Hörfassung für Blinde"
      else:       language = "Originalfassung mit Untertiteln"
      print( "Sprache:        " + language )

      print( "Bitrate:        %d" % stream['bitrate'] )
      print( "Breite x Höhe:  %d x %d" % (stream['breite'], stream['hoehe']) )
      if 'rtmp' in stream: print( "Rtmp Server:    " + stream['rtmp'] )
      print( "Url:            " + stream['url'] )

  def get_video_filename(self, sendezeit, titel):
    titel = re.sub(' -{1,2} ', '--', titel)
    return super().get_video_filename(sendezeit, titel)

  def print_video_info(self):
    if not self.json_is_parsed: self.parse_json()
    if not self.html_is_parsed: self.parse_html()

    video_info = self.video_info
    titel = video_info['titel']
    if video_info['titel_zusatz']: titel += " -- " + video_info['titel_zusatz']
    print( titel )
    print( "\nId:                 " + video_info['hash'] )
    print( "Kategorie:          " + video_info['art'] )

    tw = TermiTable.get_term_width()
    lines = []
    line = "Stichworte:         "
    for word in video_info['keywords'].split(','):
      if len(line) + len(word) + 1 > tw:
        lines.append(line)
        line = "                    "
      line += word + ","
    lines.append(line[0:-1])
    for line in lines: print(line)

    sendezeit = video_info['zeit_genau']
    print( "Sendezeit:          " + sendezeit )
    print( "Dauer der Sendung:  " + video_info['dauer'] )

    fsk = video_info['adult'] if video_info['adult'] else "0"
    print( "FSK vorhanden?:     " + fsk )

    print( "\nDownload nach:\n" + self.get_video_filename(sendezeit, titel) )

    print( "\nInhalstangabe:\n" + video_info['inhalt'] )
    print( "\nBild:\n" + video_info['bild'] )
    print( "\nLink zur Sendung in der Arte Mediathek:\n" + video_info['url'] )

  """ invoke that only after get_video_streams_for_download ! """
  def get_video_filename_for_download(self):
    if not self.json_is_parsed: self.parse_json()
    video_info = self.video_info
    titel = video_info['titel']
    if video_info['titel_zusatz']: titel += " -- " + video_info['titel_zusatz']
    return self.get_video_filename(video_info['zeit_genau'], titel)

  def get_video_streams_for_download(self, streams):
    try:
      if not 'streams' in self.video_info: self.get_video_streams()
    except ArteFskException as msg:
      print(msg)
      return MediathekDownload.DOWNLOAD_NO_STREAM_FSK
    if not self.json_is_parsed: self.parse_json()

    for i in reversed(range(len( self.video_info['streams'] ))):
      streams.append( self.video_info['streams'][i] )

    return 0

  def test_can_download( self, stream ): return True

  def print_meta_info( self ):
    print( self.video_hash )
    print( self.filename )
