from .base import Mediathek
from .broadcast import Broadcast
from mediathek_dl.tools import TermiTable

import re

from lxml import html
from lxml.html import builder as E

import urllib.error
import urllib.request

from mediathek_dl.info import ARDInfo

class ARDMediathek(Mediathek):
  def __init__(self):
    super().__init__()
    self.path_broadcast_db = "ard.ini"
    self.url_base = 'http://www.ardmediathek.de'
    self.page_id_sendung_verpasst = None

    self.body = E.BODY()
    self.htmlout = E.HTML( E.HEAD(
      E.LINK( rel="stylesheet", href="ard.css", type="text/css") ),
      self.body
    )

    # configuration for printing of broadcast list
    self.info_cols = [ 'id', 'zeit', 'thema', 'dauer', 'titel', 'kanal' ]

  def get_page_id_sendung_verpasst(self):
    ures = urllib.request.urlopen( self.url_base ).read().decode('utf8')
    url_match = re.search('<a data-theme="b" href="([^"]*)', ures)
    url = url_match.group(1)

    ures = urllib.request.urlopen( self.url_base + url ).read().decode('utf8')
    id_match = re.search('pageId=([^&>]*)&', ures)
    self.page_id_sendung_verpasst = id_match.group(1)
    return self.page_id_sendung_verpasst

  def update_broadcast_database(self):
    print("Beginne Update der Sendedaten")
    try:
      channels = self.get_channel_list()
      for day in range(7):
        print( "Lade Sendedaten Tag " + str(day+1) + " von 7" )
        for channel in channels:
          self.read_broadcasts_list(self.broadcast_db, day, channel)
      print("Alle Sendedaten wurden gelesen!")
    except urllib.error.URLError:
      print("Probleme beim Update der Sendedaten!")
      print("Update unvollständig!")
      print("Besteht die Verbindung zum Internet?")
    with open(self.path_broadcast_db, 'w') as dbfile:
      self.broadcast_db.write(dbfile)

  def get_channel_list( self ):
    print("Erstelle Liste der Sender.")

    channels = [ {"kanal" : None, "name" : "ARD" } ]
    html_data = self.get_html_data(0)
    html_root = html.fromstring(html_data)
    html_root = html_root.xpath('//div[@class="entryGroupWrapper"]')[0]
    for channel in html_root.find_class('entry'):
      link = channel.xpath('.//a')[0]
      match = re.search( 'kanal=(.*)', str(link.get('href')) )
      if not match: continue
      channel = " ".join(link.text_content().split())
      channels.append( {"kanal" : match.group(1), "name" : channel} )

    print("Liste der Sender geladen.")

    return channels

  def read_broadcasts_list( self, broadcast_db, day, channel ):
    base_str = "  Lade Sendedaten zu Kanal " + channel['name']

    print(base_str + " - downloading...", end="")
    page_id = self.page_id_sendung_verpasst
    html_data = self.get_html_data(day, channel["kanal"])

    print("\r" + base_str + " - parsing...    ", end="")

    if re.search('</body>', html_data) == None:
      raise Exception("Parse Error: Kein </body> tag gefunden, Abbruch!")

    count = 0
    html_root = html.fromstring(html_data)
    html_root = html_root.xpath('//div[@class="boxCon isCollapsible"]')
    if len(html_root) != 0:
      for teaser in html_root[0].find_class('teaser'):
        count += 1
        self.read_broadcast_dom( teaser, day, channel['name'], broadcast_db )

    print("\r" + base_str + " - done ("+str(count)+")       ")

  def read_broadcast_dom( self, teaser, day, channel, broadcast_db ):
    entry    = teaser.getparent().getparent().getparent()

    link     = teaser.find_class('mediaLink')[0].get('href')

    match    = re.search( 'documentId=(.*)&bcastId', link )
    key      = match.group(1)

    category = entry.find_class("titel")[0].text_content()
    category = re.sub('\n', ' ', category)
    time     = entry.find_class("date")[0].text_content()

    img      = teaser.xpath('.//img[@class="img hideOnNoScript"]')[0]
    img      = img.get('data-ctrl-image')
    match    = re.search( '(/image/.*)##width##', img )
    img      = match.group(1)

    title    = teaser.find_class('headline')[0].text_content()
    title    = re.sub('', '-', title)

    if len(teaser.find_class('subtitle')) > 0:
      duration = teaser.find_class('subtitle')[0].text_content()[:5]
    else: duration = ""

    broadcast_db[key] = {}

    broadcast_db[key]['id'] = key
    broadcast_db[key]['hash'] = key
    broadcast_db[key]['thema'] = category
    broadcast_db[key]['titel'] = title
    broadcast_db[key]['kanal'] = channel
    broadcast_db[key]['zeit'] = str(self.get_date(-day)) + ' ' + time
    broadcast_db[key]['dauer'] = duration
    broadcast_db[key]['link'] = link
    broadcast_db[key]['bild'] = img

    if False:
      self.body.append(
        E.DIV(
          E.CLASS("entry"),
          E.A(
            E.IMG( E.CLASS("image"), src=(self.url_base + img + '320') ),
            "Kategorie: " + category, E.BR,
            "Titel: " + title, E.BR,
            "Dauer: " + duration, E.BR,
            "Sendezeit: " + str(self.get_date(-day)) + ' ' + time, E.BR,
            href=(self.url_base + link)
          )
        )
      )

  def read_broadcast_from_database(self, section):
    bc = Broadcast(self.broadcast_db, section)
    bc.add_col( 'hash', bc.get_col('id') )
    bc.add_col( 'mediathek', 'ARD' )
    return bc

  def debug_print_broadcast( self, bc, print_sep ):
    print("docId      = " + bc['docId'])
    print("pageId     = " + bc['pageId'])
    print("titel      = " + bc['titel'])
    print("thema      = " + bc['thema'])
    print("thema_lang = " + bc['thema_lang'])
    print("kanal      = " + bc['kanal'])
    print("zeit       = " + bc['zeit'])
    print("dauer      = " + bc['dauer'])
    print("bild_klein = " + bc['bild_klein'])

    if print_sep: print( "=" * TermiTable.get_term_width() )

  def get_html_data( self, day, channel=None ):
    url = self.url_base + '/tv/sendungVerpasst?tag=' + str(day)
    if channel: url += '&kanal=' + str(channel)

    try:
      ures = urllib.request.urlopen( url ).read().decode('utf8')
    except urllib.error.HTTPError:
      date = self.get_date( -day )
      print("  Die Sendungen vom %s sind momentan nicht verfügbar!" % date)
      print("  Versuchen Sie es später erneut!")
      ures = None

    return ures

  def get_webdriver_for_page_id( self, page_id, date, browser ):
    self.init_parser() # define some otherwise undefined entities
    dstr1 = '/Sendungen-vom-{:%d-%m-%Y}'.format(date)
    dstr2 = '&date={:%d.%m.%Y}'.format(date)
    url = self.url_base + dstr1 + '?pageId=' + page_id + dstr2

    try:
      browser.get(url)
    except urllib.error.HTTPError:
      print("  Die Sendungen vom %s sind momentan nicht verfügbar!" % date)
      print("  Versuchen Sie es später erneut!")
      return None

    return browser

  def print_broadcasts(self, broadcasts, sort_after):
    if TermiTable.get_term_width() < 110:
      self.body_cols = [ 'id', 'zeit', 'thema', 'titel', 'dauer' ]
      self.head_cols = [ "Id", "Zeit", "Thema",  "Titel", "Dauer" ]
      self.resizable_cols = [ 2, 3 ]
      self.align_head = ["-"] * 5
      self.align_body = ["-"] * 5
    else:
      self.body_cols = ['id','zeit','kanal','thema','titel','dauer']
      self.head_cols = [ "Id", "Zeit", "Kanal", "Thema", "Titel", 'Dauer' ]
      self.resizable_cols = [ 3, 4 ]
      self.align_head = ["-"] * 6
      self.align_body = ["-"] * 6
    super().print_broadcasts(broadcasts, sort_after)

  def print_broadcasts_info(self, broadcasts):
    ardinfo = ARDInfo( broadcasts[0] )
    ardinfo.print_video_info()
    tw = TermiTable.get_term_width()
    for bc in broadcasts[1:]:
      print('\n' + tw*'=' + '\n')
      ardinfo.set_broadcast( bc )
      ardinfo.print_video_info()
