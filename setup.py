# Always prefer setuptools over distutils:
from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the relevant file
with open(path.join(here, 'DESCRIPTION.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='mediathek-dl',
    version='1',
    description='Examine and Download from Mediatheken',
    long_description=long_description,
    author='Some Guy',
    author_email='none@none.none',
    license='GPLv3',
    keywords='mediatheken download broadcasts video german television tv',
    packages=['mediathek_dl']
)
