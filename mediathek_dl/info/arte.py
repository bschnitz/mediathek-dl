class ARTEInfo():
  def __init__(self, broadcast):
    self.base_addr = 'http://www.arte.tv/guide/de'
    self.path_broadcast_db = 'arte.ini'
    super().__init__( video_hash )

  def parse_html(self):
    if self.html_is_parsed: return True

    if not os.path.exists( self.html_output_file ):
      self.download_html( self.video_info['url'], self.html_output_file )

    flags = re.MULTILINE | re.DOTALL
    re_description = '<section[^>]*action="description"[^>]*>(.*?)</section>'
    with open(self.html_output_file, "r") as html_data:
      html_source = html_data.read()
      self.video_info['inhalt']       = self.get_long_description(html_source)
      self.video_info['json']         = self.get_json_addr(html_source)

    self.html_is_parsed = True

  def parse_json(self):
    if self.json_is_parsed: return True

    if not self.json_is_loaded: self.load_json()

    key_sec = 'videoDurationSeconds'
    self.video_info['titel_zusatz']      = self.json_get('VSU')
    self.video_info['art']               = self.json_get('VCG')
    self.video_info['dauer_in_sekunden'] = self.json_get('videoDurationSeconds')
    self.video_info['zeit_genau']        = self.json_get('VRA')

    keywords = set()
    for key in [ "VCH", "VCM" ]:
      for entry in self.json_get(key):
        keywords = keywords | set(re.split(' / | & ', entry['label']))

    self.video_info['keywords'] = ','.join(keywords)

    self.json_is_parsed = True
    return True

  def print_video_info(self):
    self.parse_json()
    self.parse_html()

    video_info = self.video_info
    titel = video_info['titel']
    if video_info['titel_zusatz']: titel += " -- " + video_info['titel_zusatz']
    print( titel )
    print( "\nId:                 " + video_info['hash'] )
    print( "Kategorie:          " + video_info['art'] )

    tw = TermiTable.get_term_width()
    lines = []
    line = "Stichworte:         "
    for word in video_info['keywords'].split(','):
      if len(line) + len(word) + 1 > tw:
        lines.append(line)
        line = "                    "
      line += word + ","
    lines.append(line[0:-1])
    for line in lines: print(line)

    sendezeit = video_info['zeit_genau']
    print( "Sendezeit:          " + sendezeit )
    print( "Dauer der Sendung:  " + video_info['dauer'] )

    print( "\nDownload nach:\n" + self.get_video_filename(sendezeit, titel) )

    print( "\nInhalstangabe:\n" + video_info['inhalt'] )
    print( "\nBild:\n" + video_info['bild'] )
    print( "\nLink zur Sendung in der Arte Mediathek:\n" + video_info['url'] )

  def get_video_id(self, video_hash):
    self.open_broadcast_db()
    video_ids = self.broadcast_db['keymap'][video_hash].split(',')
    assert( len(video_ids) == 1 )
    return video_ids[0]

  def set_video_id(self, video_hash):
    self.have_bc_db = False
    self.video_id = self.get_video_id(video_hash)
    self.video_hash = video_hash
    self.html_is_parsed = False
    self.json_is_parsed = False
    self.json_is_loaded = False
    self.video_info = self.get_video_info(self.video_id)
    self.video_streams = None
    id_as_filename = re.sub( '/', '_', self.video_id )
    self.html_output_file = "files/arte/" + id_as_filename + ".html"
    self.json_output_file = "files/arte/" + id_as_filename + ".json"
    self.img_output_file  = "files/arte/" + id_as_filename + ".jpg"

  def get_video_info(self, video_id):
    self.open_broadcast_db()

    video_info = {}
    video_info['key']      = video_id
    video_info['hash']     = self.broadcast_db[video_id]['hash']
    video_info['url']      = self.base_addr + "/" + video_id
    video_info['titel']    = self.broadcast_db[video_id]['titel']
    video_info['dauer']    = self.broadcast_db[video_id]['dauer']
    video_info['kurztext'] = self.broadcast_db[video_id]['kurztext']
    video_info['bild']     = self.broadcast_db[video_id]['bild']
    video_info['thema']    = self.broadcast_db[video_id]['thema']
    video_info['zeit']     = self.broadcast_db[video_id]['zeit']

    return video_info

  def save_video_data(self):
    self.open_broadcast_db()
    self.image_url = dest_db[self.video_id]['bild']
    self.files = [ self.html_output_file, self.json_output_file ]
    super().save_video_data()

  def download_html( self, url, dest ):
    print("Downloading Video HTML file.")
    urllib.request.urlretrieve( url, dest )
    print("Download of HTML file is complete.")

  def get_short_description(self, html_root):
    xpath = '//section[@id="details-description"]//p[@class="description"]'
    tag = html_root.xpath(xpath)[0]
    match = re.search('\S.*\S', tag.text_content(), re.MULTILINE | re.DOTALL)
    return match.group(0)

  def get_long_description(self, html_data):
    html_root = html.fromstring(html_data)
    try:
      tag = html_root.xpath('//div[@data-action="description"]')[0]
      match = re.search('\S.*\S', tag.text_content(), re.MULTILINE | re.DOTALL)
      return match.group(0)
    except IndexError:
      return self.get_short_description(html_root)

  def get_json_addr(self, html_data):
    match = re.search("'([^']*ALL.json)'", html_data, re.MULTILINE | re.DOTALL)
    return match.group(1)

  def download_json(self):
    if not self.html_is_parsed: self.parse_html()

    response = urllib.request.urlopen( self.video_info['json'] )

    self.json_data = json.loads( response.read().decode('utf-8') )

    with open(self.json_output_file, "w") as json_file:
      json_string = json.dumps(self.json_data, indent=4, separators=(',', ': '))
      json_file.write(json_string)

    self.json_is_loaded = True

  def load_json(self):
    if not os.path.exists( self.json_output_file ):
      self.download_json()
      return

    with open(self.json_output_file) as json_file:
      self.json_data = json.loads(json_file.read())

    self.json_is_loaded = True

  def json_get(self, *args):
    if not self.json_is_loaded: self.load_json()

    json_data = self.json_data['videoJsonPlayer']
    for arg in args:
      if arg in json_data: json_data = json_data[arg]
      else:                return None
    return json_data

  def print_json(self, json_data, *args):
    print(args)
    for arg in args:
      if arg in json_data: json_data = json_data[arg]
      else:                return
    print( json.dumps(json_data, indent=4, separators=(',', ': ')) )

  def debug_examine_json(self):
    if not self.json_is_loaded: self.load_json()

    #self.print_json(self.json_data['videoSearchParams'])
    #self.print_json(self.json_data['videoJsonPlayer'])
    self.print_json(self.json_data, 'videoJsonPlayer', 'VSR')
    print()
    self.print_json(self.json_data, 'videoJsonPlayer', 'VTU', 'IUR')
    print()
    self.print_json(self.json_data, 'videoJsonPlayer', 'V7T')
    print()
    self.print_json(self.json_data, 'videoJsonPlayer', 'programImage')
    print()
    self.print_json(self.json_data, 'videoJsonPlayer', 'VCG')
    print()
    self.print_json(self.json_data, 'videoJsonPlayer', 'videoDurationSeconds')
    print()
    self.print_json(self.json_data, 'videoJsonPlayer', 'VSU')
    print()
    self.print_json(self.json_data, 'videoJsonPlayer', 'VRU')
    print()
    self.print_json(self.json_data, 'videoJsonPlayer', 'VRA')

  def get_video_streams(self):
    self.download_json()

    self.video_info['streams'] = []
    streams = self.json_get('VSR')
    for quality in streams:
      stream = streams[quality]
      s_info = {}

      s_info['server']  = stream['streamer'] if 'streamer' in stream else None
      s_info['breite']  = stream['width']
      s_info['hoehe']   = stream['height']
      s_info['bitrate'] = stream['bitrate']
      s_info['sprache'] = stream['versionShortLibelle']
      s_info['url']     = stream['url']

      if s_info['server']: s_info['url'] = 'mp4:' + s_info['url']

      self.video_info['streams'].append(s_info)

    def stream_sort_code(stream):
      lang_map = { 'DE':3, 'OmU':2, 'FR':1, 'HÖR':0 }
      lang_idx = lang_map[stream['sprache']]
      br = int(stream['bitrate'])
      width = int(stream['breite'])
      return "%d%4d%4d" % (lang_idx, br, width)
    self.video_info['streams'].sort(key=stream_sort_code)

    self.have_video_streams = True

  def print_video_streams(self):
    if not 'streams' in self.video_info: self.get_video_streams()

    for stream in self.video_info['streams']:
      print()

      if   stream['sprache'] == 'DE':  language = "Deutsch"
      elif stream['sprache'] == 'FR':  language = "Französisch"
      elif stream['sprache'] == 'HÖR': language = "Hörfassung für Blinde"
      else:       language = "Originalfassung mit Untertiteln"
      print( "Sprache:        " + language )

      print( "Bitrate:        %d" % stream['bitrate'] )
      print( "Breite x Höhe:  %d x %d" % (stream['breite'], stream['hoehe']) )
      if stream['server']: print( "Rtmp Server:    " + stream['server'] )
      print( "Url:            " + stream['url'] )

  def get_video_filename(self, sendezeit, titel):
    titel = re.sub(' -{1,2} ', '--', titel)
    return super().get_video_filename(sendezeit, titel)

  def download_video(self, resume=False, trickle=None):
    if not 'streams' in self.video_info: self.get_video_streams()
    if not self.json_is_parsed: self.parse_json()

    video_info = self.video_info
    titel = video_info['titel']
    if video_info['titel_zusatz']: titel += " -- " + video_info['titel_zusatz']
    filename = self.get_video_filename(video_info['zeit_genau'], titel)

    streams = self.video_info['streams']
    for i in reversed(range(len( streams ))):
      rv = self.download_stream(streams[i], filename, resume, trickle)
      if rv == 0:  return True  # download complete
      elif rv > 0: return False # error downloading / user abort
      else:        continue     # error, but not a user abort, try next stream
    return False                # tried every stream without success

  def download_stream(self, stream, destfile, resume=False, trickle=None):
    args  = [ 'trickle', '-d', str(trickle) ] if trickle != None else []

    if stream['server'] != None:
      destfile += '.mp4'
      args += [ "rtmpdump", '--rtmp', stream['server'],
                            '--flv', self.dl_temp_folder + destfile,
                            '--playpath', stream['url'] ]
      args += [ '--resume' ] if resume else []
    elif re.match( 'http', stream['url'] ):
      destfile += re.search( '.*(\.[^.]*)$', stream['url'] ).group(1)
      args += [ 'wget' ]
      args += [ '-c' ] if resume else []
      args += [ stream['url'], '-O', self.dl_temp_folder + destfile ]
    else: return -1 # Don't know how to download

    print("Download nach: '" + self.dl_video_folder + destfile + "'")
    self.filename = destfile
    try:
      return subprocess.call( args )
    except KeyboardInterrupt:
      return 8 
