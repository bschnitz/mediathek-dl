from mediathek_dl.download import ARDDownload, ZDFDownload, ARTEDownload

def get_mediathek_dl( sendung_id, channel ):
  if channel == 'zdf':  return  ZDFDownload(sendung_id)
  if channel == 'ard':  return  ARDDownload(sendung_id)
  if channel == 'arte': return ARTEDownload(sendung_id)
