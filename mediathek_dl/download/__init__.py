from .ard import ARDDownload
from .zdf import ZDFDownload
from .arte import ARTEDownload
from .base import MediathekDownload
