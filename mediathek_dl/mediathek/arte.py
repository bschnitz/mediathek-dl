from .base import Mediathek
from .broadcast import Broadcast
from mediathek_dl.tools import TermiTable
from mediathek_dl.download import ARTEDownload

import re
import json
import hashlib
import datetime
import urllib.request

class ARTEMediathek(Mediathek):
  def __init__(self):
    super().__init__()
    self.path_broadcast_db = "arte.ini"
    self.url_base = 'http://www.arte.tv/guide/de'
    self.len_base = len(self.url_base)
    self.url_json = self.url_base + '/plus7/videos?page=%d&limit=%d&sort=newest'
    self.page_id_sendung_verpasst = None

    # configuration for printing of broadcast list
    self.info_cols = [ 'hash', 'zeit', 'thema', 'kurztext', 'dauer', 'titel' ]

  def load_json_broadcasts_page(self, page):
    limit = 100
    url = self.url_json % (page, limit)
    response = urllib.request.urlopen( url )
    return json.loads( response.read().decode('utf-8') )
    

  def load_json_broadcasts(self):
    page = 1
    has_more = True
    videos = []
    while has_more:
      print("Lade Seite %d" % page)
      json = self.load_json_broadcasts_page(page)
      videos.extend(json['videos'])
      has_more = json['has_more']
      page=page+1
    return videos

  def get_bc_key(self, video):
    assert( video['url'].rfind(self.url_base) == 0 )
    return video['url'][self.len_base+1:]

  def get_bc_hash(self, bc_key):
    return hashlib.md5(bc_key.encode("utf-8")).hexdigest()[0:8]

  def get_bc(self, video):
    bc = {}

    bc['key']          = self.get_bc_key(video)
    bc['hash']         = self.get_bc_hash(bc['key'])
    bc['titel']        = video['title']
    if video['subtitle']:
      bc['untertitel'] = video['subtitle']
    bc['dauer']        = str(round(int(video['duration'])/60)) + " min"
    bc['kurztext']     = video['teaser']
    bc['adult']        = video['adult']
    bc['zeit']         = video['scheduled_on'] + " 00:00"
    bc['bild']         = video['thumbnail_url']

    return bc

  def update_broadcast_database(self):
    print("Lade Sendedaten aus der ARTE Mediathek")

    videos = self.load_json_broadcasts()

    self.broadcast_db['keymap'] = {}
    for vid in videos:
      bc = self.get_bc(vid)
      key = bc.pop('key')

      hash = bc['hash']
      if hash in self.broadcast_db['keymap']:
        self.broadcast_db['keymap'][hash] += ',' + key
      else: self.broadcast_db['keymap'][hash] = key

      self.broadcast_db[key] = bc

    with open(self.path_broadcast_db, 'w') as dbfile:
      self.broadcast_db.write(dbfile)

    print("Sendedaten geladen")

  def parse_adate(self, airdate_long):
    weekday, date = airdate_long.split(', ')
    date = datetime.datetime.strptime(date, '%d. %B um %H:%M Uhr')
    dnow = datetime.datetime.now()
    if date.month > dnow.month: date = date.replace( year=dnow.year-1 )
    else:                       date = date.replace( year=dnow.year   )
    return date.__str__()[0:16]

  def print_broadcasts(self, broadcasts, sort_after):
    if TermiTable.get_term_width() < 130:
      self.body_cols = [ 'hash', 'zeit', 'thema', 'dauer', 'titel' ]
      self.head_cols = [ "Id", "Zeit", "Thema", "Dauer", "Titel" ]
      self.resizable_cols = [ 2, 4 ]
      self.align_head = ["-"] * 5
      self.align_body = [ "", "", "-", "", "-" ]
    else:
      self.body_cols = [ 'hash', 'zeit', 'thema', 'kurztext', 'dauer', 'titel' ]
      self.head_cols = [ "Id", "Zeit", "Thema",
                         "Kurzbeschreibung", "Dauer", "Titel" ]
      self.resizable_cols = [ 2, 3, 5 ]
      self.align_head = ["-"] * 6
      self.align_body = [ "", "", "-", "-", "-", "-" ]
    super().print_broadcasts(broadcasts, sort_after)

  def print_broadcasts_info(self, broadcasts):
    artedl = ARTEDownload( broadcasts[0].get_col('hash') )
    artedl.print_video_info()
    tw = TermiTable.get_term_width()
    for bc in broadcasts[1:]:
      print('\n' + tw*'=' + '\n')
      artedl.set_video_id( bc.get_col('hash') )
      artedl.print_video_info()

  def read_broadcast_from_database(self, section):
    if section == "keymap": return None
    bc = Broadcast(self.broadcast_db, section)
    bc.add_col( 'mediathek', 'ARTE' )
    return bc
