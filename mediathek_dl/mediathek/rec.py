from .base import Mediathek
from .broadcast import Broadcast
from mediathek_dl.mediathek.config import Config

class RecMediathek(Mediathek):
  def __init__(self):
    super().__init__()
    self.cfg = Config()
    self.path_broadcast_db =self.cfg["General"].get("records_db", "records.ini")

    self.body_cols = [ 'mediathek', 'hash', 'status', 'versuch', 'filename' ]
    self.info_cols = self.body_cols
    self.head_cols = [ "From", "Id", "Status", "Try",  "Dateiname" ]
    self.resizable_cols = [ 2, 4 ]
    self.align_body = [ "-", "", "-", "", "-" ]
    self.align_head = ["-"] * 5
    self.default_search_in = [ 'filename' ]
    self.default_sort_after = [ 'versuch', 'zeit', 'status', 'filename' ]

  def read_broadcast_from_database(self, section):
    bc = Broadcast(self.broadcast_db, section)
    channel = section.split('#', 1)[0]
    bc.add_col('mediathek', channel)
    versuch = bc.get_col("versuch")
    versuch = 0 if versuch == None else int(versuch)
    bc.add_col('versuch', str(versuch))
    return bc
