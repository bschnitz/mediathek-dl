import os

from argparse import Namespace

from configparser import ConfigParser

from mediathek_dl.tools import Connector
from mediathek_dl.mediathek import Config
from mediathek_dl.download import MediathekDownload
from mediathek_dl.mediathek import ARDMediathek, ZDFMediathek, ARTEMediathek
from mediathek_dl.mediathek.other import get_mediathek_dl

class Recorder():
  def __init__(self, args):
    self.args = args
    self.cfg = Config()
    entry = self.cfg["General"]
    self.rec_db_path = self.get_from_cfg( entry, "records_db", "records.ini" )
    self.records = None
    self.max_tries = 99
    self.must_exit = False

  def start( self ):
    while not self.must_exit:
      rec_ident = self.get_next_record()
      if rec_ident == None: break
      rval = self.record( rec_ident )
      self.save_recording_status( rec_ident, rval )

  def get_from_cfg( self, cfg, section, default ):
    if section in cfg: return cfg[section]
    return default

  def increase_try_number( self, record ):
    versuch = int( self.get_from_cfg(record, "versuch", 0) ) + 1
    if versuch >= self.max_tries:
      versuch = self.max_tries
      record["status"] = "halted"
    Format = "%0" + str(len(str(self.max_tries))) + "d"
    record["versuch"] = Format % versuch

  def save_recording_status( self, rec_ident, exit_code ):
    records = self.reload_records()
    record = records[rec_ident]
    self.increase_try_number( record )
    if "exit_codes" in record:
      record["exit_codes"] += "," + str(exit_code)
    else: record["exit_codes"] = str(exit_code)
    while True: # switch exit_code
      if exit_code == MediathekDownload.DOWNLOAD_OK:
        record['status'] = "done"
        break
      if exit_code == MediathekDownload.DOWNLOAD_FLAWED:
        record['restart'] = "true"
        break
      if exit_code == MediathekDownload.DOWNLOAD_NO_STREAM:
        record['status'] = "halted"
        break
      if exit_code == MediathekDownload.DOWNLOAD_USER_ABORT:
        self.must_exit = True
      record['status'] = "interrupted"
      break
    self.save_records( records )

  def save_records( self, records=None ):
    if records == None: records = self.get_records()
    with open( self.rec_db_path, "w" ) as dbfile:
      records.write( dbfile )

  def record( self, rec_ident ):
    c = Connector()
    c.set_wait_for_connection_msg("%s - waiting for internet connection.")
    while True:
      c.wait_until_connected( True, 1 )
      channel, bc_ident = rec_ident.split("#", 1)
      rec = self.reload_records()[rec_ident]
      rec['status'] = "running"
      self.save_records()
      args = Namespace()
      args.restart = ('restart' in rec) and (rec['restart'] == "true")
      if ('trickle' in rec) and (rec['trickle'] == 'true'):
        args.trickle = rec['trickle']
      else: args.trickle = None
      dl = get_mediathek_dl( rec['hash'], channel )
      exit_code = dl.downloader( args )
      if exit_code != MediathekDownload.DOWNLOAD_OK:
        if c.is_connected(): # if internet connection went down, wait until
          break              # the connection is reestablished and try again
      else: break

    return exit_code

  def get_records( self ):
    if self.records == None:
      self.records = ConfigParser(interpolation=None)
      self.records.read( self.rec_db_path )
    return self.records

  def reload_records( self ):
    self.records = ConfigParser(interpolation=None)
    self.records.read( self.rec_db_path )
    return self.records

  def get_sorted_records( self, records ):
    def record_cmp_key( section ):
      secname = section[0]
      section = section[1]
      if secname == "DEFAULT":
        return str(self.max_tries)
      if section['status'] == "done" or section['status'] == "halted":
        return str(self.max_tries)
      if not "versuch" in section:
        section["versuch"] = len(str(self.max_tries)) * "0"
      return section["versuch"] + "-" + section["zeit"]

    return sorted( self.get_records().items(), key=record_cmp_key )

  def get_next_record( self ):
    """ returns the section name of the next recording, if existing, or None """
    records = self.reload_records()
    record = self.get_sorted_records( records )[0]
    record_is_valid = (
      record[0] != "DEFAULT" and
      record[1]['status'] != "done" and
      record[1]['status'] != "halted" and
      int(record[1]['versuch']) < self.max_tries
    )
    return record[0] if record_is_valid else None

  def clearrec( self ):
    if( not os.path.isfile(self.rec_db_path) ):
      print( "Aufnahmedatenbanke nicht gefunden. Es gibt nichts zu tun." )
      return

    records = self.get_records()
    for key in records:
      if key != 'DEFAULT' and records[key]['status'] == 'done':
          del records[key]

    self.save_records( records )
