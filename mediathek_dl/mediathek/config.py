#!/usr/bin/env python3

import os

from configparser import ConfigParser, ExtendedInterpolation

class Config():
  def __init__(self, path=None):
    if path == None: path = "."
    self.path = path
    self.filename = 'mediathek-dl.ini'
    self.read_cfg = False

  def get_filepath(self):
    return self.path + '/' + 'mediathek-dl.ini'

  def check(self):
      is_ok = False
      if os.path.isfile(self.get_filepath()):
        self.read()
        if 'Internal' in self.parser and 'type' in self.parser['Internal']:
           if self.parser['Internal']['type'] == 'download':
             is_ok = True
      return is_ok

  def init(self):
    parser = ConfigParser( interpolation=ExtendedInterpolation() )
    parser.read('mediathek-dl.ini')
    parser['Internal'] = {}
    parser['Internal']['type'] = 'download'
    with open('mediathek-dl.ini', 'w') as fp:
      parser.write(fp)

  def read(self):
    if self.read_cfg: return self
    self.read_cfg = True
    self.parser = ConfigParser( interpolation=ExtendedInterpolation() )
    self.parser.read( self.get_filepath() )
    return self

  def __getitem__( self, ident ):
    self.read()
    if not ident in self.parser: return {}
    return self.parser[ident]
    
  def check_flag( self, section, ident ):
    self.read()
    if not section in self.parser: return False
    if not ident in self[section]: return False
    return self[section][ident] == "True" or self[section][ident] == "true" or \
           self[section][ident] == "yes"  or self[section][ident] == "Yes" or \
           self[section][ident] == "1"
