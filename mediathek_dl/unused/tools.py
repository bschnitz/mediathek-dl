def get_html_tag(html, name, prop, prop_value):
  pattern_start_tag = "<\s*" + name + "\s*[^>]*"\
                             + prop + "\s*=\s*['\"]"\
                             + prop_value + "['\"][^>]*>"
  pattern_tag = "<[/ ]*" + name + "[^>]*>"
  pattern_is_endtag  = "^< */"

  m = re.search(pattern_start_tag, html)

  if not m: return None

  match.start = m.start(0)
  match.end = m.end(0)
  match.str = m.group(0)

  if( re.match(pattern_is_endtag, m.group(0)) ): return match

  start_inner = m.end(0)

  rest = html
  number_of_open_tags = 1
  while(m):
    rest = rest[m.end(0)+1:]
    m = re.search(pattern_tag, rest)
    if( re.match(pattern_is_endtag, m.group(0)) ): number_of_open_tags -= 1
    else:                                          number_of_open_tags += 1
    match.end += m.end(0) + 1
    if number_of_open_tags == 0:
      end_inner = match.end - (m.end(0) - m.start(0))
      break

  match.str = html[match.start:match.end]
  match.inner = html[start_inner:end_inner]

  return match
