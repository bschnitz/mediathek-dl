import os
import shutil
import configparser
from mediathek_dl.mediathek import Config
from mediathek_dl.tools import mergedir

class Merge():
  def __init__(self, args):
    self.args = args
    self.broadcast_inis = ['arte.ini', 'zdf.ini', 'ard.ini']
    if not Config(self.args.dest).check():
      print(
          "Zielverzeichnis ist keine Mediathek Download Verzeichnis. Abbruch."
      )
    else:
      have_collisions = False
      os.makedirs(self.args.dest + '/downloads/videos', exist_ok=True)
      if not self.args.force:
        for ini in self.broadcast_inis:
          if self.check_for_collisions(ini):
            have_collisions = True
      if not have_collisions:
        for ini in self.broadcast_inis:
          self.merge_broadcast_ini(ini)
        print("copying files now.")
        mergedir( 'downloads/files', self.args.dest + '/downloads/files' )
        print("copying videos now.")
        mergedir( 'downloads/videos', self.args.dest + '/downloads/videos' )

  def open_dbs(self, which):
    self.src_db = configparser.ConfigParser(interpolation=None)
    self.dst_db = configparser.ConfigParser(interpolation=None)
    self.src_db.read('downloads/' + which)
    self.dst_db.read(self.args.dest + '/downloads/' + which)

  def check_for_collisions(self, which):
    if self.args.force: return False

    self.open_dbs(which)
    collisions = []
    if 'keymap' in self.src_db: # arte.ini
      for Hash in self.src_db['keymap']:
        if Hash in self.dst_db['keymap']:
          self.test_insert_collision( Hash, collisions )
    else: # ard.ini or zdf.ini
      for section in self.src_db:
        if section == 'DEFAULT': continue
        if section in self.dst_db:
          self.test_insert_collision( section, collisions )

    if len(collisions) and not self.args.force:
      print( "Es gibt Kollisionen für "+which+". Videos werden nicht kopiert." )
      #if not self.args.overwrite:
      if True: # TODO for overwrite
        for collision in collisions:
          print()
          print( 'key: '  + collision['key'] )
          print( 'src: '  + collision['src'] )
          print( 'dst: '  + collision['dst'] )
    else:
      return False
    return True

  def test_insert_collision(self, Hash, collisions):
    if 'keymap' in self.src_db:
      key_src = self.src_db['keymap'][Hash]
      key_dst = self.src_db['keymap'][Hash]
    else: key_src = key_dst = Hash
    title_src = self.src_db[key_src]['titel']
    title_dst = self.dst_db[key_dst]['titel']
    if (not self.args.ignore_existing) or title_src != title_dst:
      collision = {}
      collision['key'] = Hash
      collision['src'] = title_src
      collision['dst'] = title_dst
      collisions.append(collision)

  def backup_ini_file(self, which):
    i = 0
    which = which[:-4] # remove .ini extension
    backup_path = ""
    while True:
      backup_path = self.args.dest + '/' + which + '-' + str(i) + '.ini'
      if os.path.exists(backup_path): i = i + 1
      else: break
    shutil.copyfile(self.args.dest + '/' + which + '.ini', backup_path)

  def backup_files(self):
    sutil.copy2( 'downloads/files' )

  def merge_broadcast_ini(self, which):
    self.open_dbs(which)

    self.backup_ini_file('/downloads/' + which)

    if 'keymap' in self.src_db:
      if not 'keymap' in self.dst_db:
        self.dst_db['keymap'] = {}
      for key in self.src_db['keymap']:
        self.dst_db['keymap'][key] = self.src_db['keymap'][key]

    for section in self.src_db:
      if section != 'keymap': self.dst_db[section] = self.src_db[section]

    with open(self.args.dest + '/downloads/' + which, 'w') as ini:
      self.dst_db.write(ini)

    print("Successfully merged " + which)
