from .base import Mediathek
from .broadcast import Broadcast

from mediathek_dl.tools import TermiTable
from mediathek_dl.download import ZDFDownload

import re
import urllib
import xml.etree.ElementTree as ET

class ZDFMediathek(Mediathek):
  def __init__(self):
    super().__init__()
    self.path_broadcast_db = "zdf.ini"
    self.url_base = 'http://www.zdf.de/ZDFmediathek/'
    self.url_list = self.url_base + 'hauptnavigation/sendung-verpasst/';
    self.colw = None

    # configuration for printing of broadcast list
    self.info_cols = [ 'id', 'zeit', 'thema', 'dauer', 'titel' ]
    self.body_cols = [ 'id', 'zeit', 'thema', 'dauer', 'titel' ]
    self.head_cols = [ "Id", "Zeit", "Thema", "Dauer", "Titel" ]
    self.resizable_cols = [ 2, 4 ]
    self.align_head = ["-"] * 5
    self.align_body = ["", "", "-", "", "-"]

  def fetch_broadcasts_from_web(self, day):
    self.init_parser()
    response = urllib.request.urlopen( self.url_list + day )
    html_root = ET.fromstring(response.read(), self.parser)
    default_namespace = re.search('{(.*)}', html_root.tag).group(1)
    namespaces = { 'ns' : default_namespace }

    nodes_broadcast = html_root.findall(".//*[@class='text']")
    for node_broadcast in nodes_broadcast:
      nodes_info = node_broadcast.findall( "ns:p/ns:a", namespaces )
      href = nodes_info[0].attrib['href']

      id = re.search("/([0-9]*)(/|\?)", href).group(1)
      bc = Broadcast.try_add_new_broadcast( self.broadcast_db, id )
      assert bc

      bc.add_col( 'hash', id )

      thema = ""
      if re.search(",", nodes_info[0].text):
        thema, zeit = nodes_info[0].text.rsplit(", ", 1)
      else: zeit = nodes_info[0].text
      
      zeit = self.parse_time( zeit )

      bc.add_col( 'zeit', zeit )
      bc.add_col( 'thema', thema )
      bc.add_col( 'dauer', nodes_info[1].text.split(", ")[1] )
      titel = node_broadcast.find( "ns:p/ns:b/ns:a", namespaces ).text
      bc.add_col( 'titel', titel )
      self.broadcasts.append(bc)

    return self.broadcasts

  def parse_time( self, time ):
    return re.sub( r"(..)\.(..)\.(....)", r"\3-\2-\1", time )

  def update_broadcast_database(self):
    print("Beginne Update der Sendedaten")
    for nr in range(8):
      print("Lade Sendedaten Tag " + str(nr+1) + " von 8")
      self.fetch_broadcasts_from_web("day" + str(nr))
    print("Sendedaten geladen")
    with open(self.path_broadcast_db, 'w') as dbfile:
      self.broadcast_db.write(dbfile)

  def print_broadcasts_info(self, broadcasts):
    zdfdl = ZDFDownload( broadcasts[0].get_col('id') )
    zdfdl.print_video_info()
    tw = TermiTable.get_term_width()
    for bc in broadcasts[1:]:
      print('\n' + tw*'=' + '\n')
      zdfdl.set_video_id( bc.get_col('id') )
      zdfdl.print_video_info()

  def read_broadcast_from_database(self, section):
    bc = Broadcast(self.broadcast_db, section)
    bc.add_col( 'hash', bc.get_col('id') )
    bc.add_col( 'mediathek', 'ZDF' )
    return bc
