import os
import re
import shutil
import subprocess
import configparser
import urllib.request

from mediathek_dl.tools import logme
from mediathek_dl.mediathek.config import Config

verbose = 2

class MediathekDownload():
  """ return values for self.download_video """
  DOWNLOAD_OK            =  0 # download was successful
  DOWNLOAD_FLAWED        =  1 # download not complete and cannot be resumed
  DOWNLOAD_RECOVERABLE   =  2 # download not complete but may be resumed
  DOWNLOAD_NO_STREAM     =  4 # no stream found for download
  DOWNLOAD_NO_STREAM_FSK =  8 # no stream found (temporarily) due to fsk
  DOWNLOAD_USER_ABORT    = 16 # user aborted the download

  def __init__(self, video_id):
    self.cfg = Config()
    self.path_records_db = self.cfg["General"].get("records_db", "records.ini")
    self.set_video_id(video_id)
    self.translate = { ord('ä'):'ae', ord('ü'):'ue', ord('ö'):'oe',
                       ord('Ä'):'Ae', ord('Ü'):'Ue', ord('Ö'):'Oe',
                       ord('ß'):'ss',
                       ord(':'):'-', ord(' '):'-', ord('.'):'-', ord("'"):'-',
                       ord(','):'-',
                       ord('&'):'and',
                       ord('"'):None,
                       ord('('):None, ord(')'):None,
                       ord('!'):None, ord('?'):None  }
    self.dl_folder = 'downloads/'
    self.dl_video_folder = self.dl_folder + 'videos/'
    self.dl_temp_folder = self.dl_video_folder + 'laufend/'
    self.have_bc_db = False

  def set_video_id(self, video_id):
    self.have_bc_db = False
    self.video_id = video_id

  def open_broadcast_db(self):
    if not self.have_bc_db:
      self.broadcast_db = configparser.ConfigParser(interpolation=None)
      self.broadcast_db.read(self.path_broadcast_db)
      self.have_bc_db = True

  def get_video_filename(self, zeit, title):
    title = self.clean_video_filename(title)
    zeit = self.rework_date(zeit[0:10])
    return zeit + '--' + title

  def clean_video_filename(self, filename):
    filename = filename.translate( self.translate )
    return re.sub("/", "-von-", filename)

  def rework_date(self, date):
    matches = re.match('([0-9]{2}).([0-9*]{2}).([0-9]{4})', date)
    return   matches.group(3).translate( {ord('.'):'-'} ) + '-' \
           + matches.group(2) + '-' \
           + matches.group(1)

  def make_directories(self):
    os.makedirs("files/ard", exist_ok=True)
    os.makedirs("files/zdf", exist_ok=True)
    os.makedirs("files/arte", exist_ok=True)
    os.makedirs("downloads/files/ard", exist_ok=True)
    os.makedirs("downloads/files/zdf", exist_ok=True)
    os.makedirs("downloads/files/arte", exist_ok=True)
    os.makedirs("downloads/videos/laufend", exist_ok=True)

  def recorder( self, args ):
    try:
      if not self.have_downloaded_video():
        self.make_directories()
        self.save_recording( args )
      else:
        print("Abbruch: Video ist bereits heruntergeladen.")
    except urllib.error.URLError as e:
      logme( str(e), 2 )
      print("Fehler beim Verbindungsaufbau. Besteht die Internetverbindung?")

  def save_recording( self, args ):
    src_db = configparser.ConfigParser(interpolation=None)
    src_db.read( self.path_broadcast_db )
    filename = self.get_video_filename_for_download()
    rec_db = configparser.ConfigParser(interpolation=None)
    rec_db.read( self.path_records_db )
    key = self.channel.lower()+"#"+self.video_id
    if (not key in rec_db) or args.restart:
      rec_db[key] = src_db[self.video_id]
      rec_db[key]['status'] = 'new'
      rec_db[key]['filename'] = self.get_video_filename_for_download()
      rec_db[key]['trickle'] = str(args.trickle) if args.trickle else "false"
    with open( self.path_records_db, 'w' ) as dbfile:
      rec_db.write( dbfile )

  def downloader( self, args ):
    try:
      if args.restart: self.mark_video_as_running()
      if not self.have_downloaded_video():
        self.make_directories()
        exitcode = self.download_video( args.restart, args.trickle )
        if exitcode != MediathekDownload.DOWNLOAD_OK:
          print("Fehler beim Download / Benuterabbruch")
        else: self.save_video_data()
      else:
        print("Abbruch: Video ist bereits heruntergeladen.")
        exitcode = MediathekDownload.DOWNLOAD_OK
    except urllib.error.URLError as e:
      logme( str(e), 2 )
      print("Fehler beim Verbindungsaufbau. Besteht die Internetverbindung?")
      exitcode = MediathekDownload.DOWNLOAD_RECOVERABLE
    return exitcode
      
  def mark_video_as_running(self):
    if os.path.isfile( self.dl_folder + self.path_broadcast_db ):
      dest_db = configparser.ConfigParser(interpolation=None)
      dest_db.read( self.dl_folder + self.path_broadcast_db )
      if self.video_id in dest_db:
        dest_db[self.video_id]['status'] = 'running'

  def have_downloaded_video(self):
    if not os.path.isfile( self.dl_folder + self.path_broadcast_db ):
      return False
    dest_db = configparser.ConfigParser(interpolation=None)
    dest_db.read( self.dl_folder + self.path_broadcast_db )
    if self.video_id in dest_db:
      if 'status' in dest_db[self.video_id]:
        return dest_db[self.video_id]['status'] == 'downloaded'
      else: return True
    else: return False

  def save_video_data(self):
    self.open_broadcast_db()

    dest_db = configparser.ConfigParser(interpolation=None)
    dest_db.read( self.dl_folder + self.path_broadcast_db )

    if hasattr( self, 'video_hash' ):
      if 'keymap' not in dest_db: dest_db['keymap'] = {}
      assert( self.video_hash not in dest_db['keymap'] )
      dest_db['keymap'][self.video_hash] = self.video_id

    dest_db[self.video_id] = self.broadcast_db[self.video_id]
    dest_db[self.video_id]['datei'] = self.filename
    dest_db[self.video_id]['status'] = 'downloaded'

    video_src = self.dl_temp_folder + self.filename
    video_dst = self.dl_video_folder + self.filename
    shutil.move( video_src, video_dst )

    for f in self.files: shutil.copy2(f, self.dl_folder + f)

    if self.image_url != None:
      img_dest = self.dl_folder + self.img_output_file
      urllib.request.urlretrieve( self.image_url, img_dest )

    with open( self.dl_folder + self.path_broadcast_db, 'w' ) as dbfile:
      dest_db.write( dbfile )

  def format_dl_cmd(self, args, linesep="\n  "):
    cmd = " ".join(args)
    break_at = [
        "-c", "-O",
        "--host", "--app", "--playpath", "--rtmp", "--resume", "--flv"
    ]
    return re.sub( " (" + "|".join(break_at) + ")", linesep+"\\1", cmd )

  def has_fsk(self, stream):
    if re.search('hinweis_default', stream['url']) or \
       re.search('fsk16', stream['url']):
      err = "Video kann nicht heruntergeladen werden.\n" \
            "(Möglicherweise ist ein Download nur zwischen "\
            "22:00 und 6:00 möglich.)"
      print( err )
      self.delete_streamfile()
      return True
    return False

  def delete_streamfile(self):
    pass

  def log_dl_cmd(self, args):
    logme( "Datei:\n" + self.filename )
    logme( "Zielpfad:\n" + self.dl_video_folder, 1 )
    logme( "Befehl:\n  " + self.format_dl_cmd(args) )

  def wrap_rtmp_cmd( self, cmd ):
    return """
    while read -r -d $\'\\r\' line; do
        if [[ "${line}" =~ \(.*%\)$ ]]; then
          echo -ne "$line\\r"
        fi
    done < <("""+cmd+" 2>&1"+")"

  def try_create_rtmp_cmd( self, stream, destfile, args, restart ):
    destfile += '.mp4'

    append = [ "rtmpdump" ]
    if   'rtmp' in stream:
      append += [ '--rtmp', stream['rtmp'], '--playpath', stream['url'] ]
    elif 'host' in stream:
      append += [ '--host', stream['host'], '--playpath', stream['url'] ]
    elif re.match( 'rtmp', stream['url'] ):
      append += [ '--rtmp', stream['url'] ]
    else:
      return None

    args += append
    args += [ '--flv', destfile ]

    if 'app' in stream: args += [ '--app', stream['app'] ]
    if not restart and os.path.isfile(destfile): args += [ '--resume' ]

    if self.cfg.check_flag( 'General', 'use_short_output' ):
      cmd = self.wrap_rtmp_cmd( " ".join(args) )
      del args[:]
      args += [ "bash", "-c", cmd ]

    return '.mp4'

  def try_create_wget_cmd( self, stream, destfile, args, restart ):
    if not re.match( 'http', stream['url'] ): return None

    extension = re.search( '.*(\.[^.]*)$', stream['url'] ).group(1)
    destfile += extension

    args += [ 'wget', '--progress=bar' ]
    args += [ '-c' ] if not restart else []
    args += [ stream['url'], '-O', destfile ]

    if self.cfg.check_flag( 'General', 'use_short_output' ):
      args += [ '-q', '--show-progress' ]

    return extension

  def download_video(self, restart=False, trickle=None):
    streams = []
    rval = self.get_video_streams_for_download(streams)
    if rval: return rval # no streams found for download

    filename = self.get_video_filename_for_download()

    for stream in streams: # take the best stream possible
      rv = self.download_stream(stream, filename, restart, trickle)
      if rv < 0: continue # don't know how to download stream, try next one
      else: return rv
    return MediathekDownload.DOWNLOAD_NO_STREAM

  def download_stream(self, stream, destfile, restart=False, trickle=None):
    args  = [ 'trickle', '-d', str(trickle) ] if trickle != None else []

    if not self.test_can_download( stream ): return -1

    tempfile = self.dl_temp_folder + destfile
    while True:
      extension = self.try_create_rtmp_cmd( stream, tempfile, args, restart )
      if extension: break
      extension = self.try_create_wget_cmd( stream, tempfile, args, restart )
      if extension: break
      return -1 # don't know how to download stream

    return self.execute_download_command( destfile + extension, args )

  def print_meta_info( self ):
    print( self.video_id )
    print( self.filename )

  def execute_download_command(self, destfile, args):
    self.filename = destfile
    self.log_dl_cmd( args )
    self.print_meta_info()
    try:
      rval = subprocess.call( args )
      logme( "Rückgabewert Subprocess: " + str(rval), 2 )
      if args[0] == 'rtmpdump' and rval == 1:
        return MediathekDownload.DOWNLOAD_FLAWED
      elif rval == MediathekDownload.DOWNLOAD_OK: return rval
    except KeyboardInterrupt:
      logme( "Benutzerabbruch während des Downloads", 2 )
    return MediathekDownload.DOWNLOAD_USER_ABORT
