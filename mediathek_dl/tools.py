import os, errno, shutil
import re
import html
import time
import socket
import datetime
import subprocess

class TagMatch():
  def __init__(self, start, end, match, inner = None):
    self.start = start
    self.end = end
    self.match = match
    self.inner = inner

  def update(self, start, end, match, inner = None):
    self.start = start
    self.end   = end
    self.match = match
    self.inner = inner

  def get_attrib_value( self, attributes_name ):
    pattern = "<[^>]*\s" + attributes_name + "\s*=\s*['\"]([^'\"]*)['\"][^>]*>"
    match = re.search( pattern, self.match )
    if match: return match.group(1)
    else:     return None

  def have_inner(self): return self.inner != None

  def get_start(self): return self.start
  def get_end(self):   return self.end

  def get_inner( self, decode_html_entities = True ):
    if decode_html_entities:
      return html.parser.HTMLParser().unescape(self.inner)
    else: return self.inner

  def get_match(self): return self.match

  def finditer(self, tag_name, prop_name = None, prop_value = None):
    assert self.have_inner()
    return HTMLTags(self.inner, tag_name, prop_name, prop_value)

  def find(self, tag_name, prop_name = None, prop_value = None):
    it = HTMLTags(self.inner, tag_name, prop_name, prop_value)
    it.__iter__()
    return it.__next__()

  def __str__(self):
    return self.match

class HTMLTags():
  """ Iterate through tags in a html string.
      
      Warning: This is not very stable. There are Bugs!
      (for example if 'prop_name' is contained in another property's value.) """

  def __init__(self, html_string, tag_name, prop_name=None, prop_value=None):
    self.html = html_string
    self.tag_name = tag_name
    self.prop_name = prop_name
    self.prop_value = prop_value

    if prop_value:
      self.pattern_tag_start = "<\s*" + tag_name + "(\s[^>]*\s|\s)"\
                                      + prop_name + "\s*=\s*['\"]"\
                                      + prop_value + "['\"][^>]*>"
    elif prop_name:
      self.pattern_tag_start = "<\s*" + tag_name + "(\s[^>]*\s|\s)"\
                                      + prop_name + "[^>]*>"
    else:
      self.pattern_tag_start = "<\s*" + tag_name + "(\s[^>]*|)>"

    self.pattern_tag = "<[/ ]*" + tag_name + "[^>]*>"
    self.pattern_is_endtag  = "^< */"
    self.match = None

  def __iter__(self):
    self.match = None
    self.miter = re.finditer( self.pattern_tag_start, self.html )
    return self

  def __next__(self):
    m = self.miter.__next__()

    start = m.start(0)
    end   = m.end(0)
    match = m.group(0)

    self.match = TagMatch(start, end, match)

    if( re.search("/\s*>$", match) ): return self.match

    start_inner = end

    rest = self.html
    num_open_tags = 1
    while(m):
      rest = rest[m.end(0)+1:]
      m = re.search(self.pattern_tag, rest)
      if( re.match(self.pattern_is_endtag, m.group(0)) ): num_open_tags -= 1
      else:                                               num_open_tags += 1
      end += m.end(0) + 1
      if num_open_tags == 0:
        end_inner = end - (m.end(0) - m.start(0))
        break

    inner = self.html[start_inner:end_inner]
    self.match.update( start, end, self.html[start:end], inner )

    return self.match

  def __getitem__(self, index):
    for tag in self:
      if not index: return tag
      index -= 1
    raise Exception("Index out of range!")

  def get(self):
    self.__iter__()
    return self.__next__().__str__()

class TermiTable():
  def __init__(self, num_cols, col_sep):
    self.col_sep = col_sep
    self.num_cols = num_cols
    self.col_info  = [{ 'size':0, 'resizable':False } for x in range(num_cols)]
    self.num_resizables = 0
    self.rows = []

  def set_alignment(self, alignments):
    if len(alignments) != self.num_cols:
      raise Exception("TermiTable: mismatch of number of columns!")
    for i in range(self.num_cols):
      self.col_info[i]['align'] = alignments[i]

  def set_align_head(self, alignments):
    if len(alignments) != self.num_cols:
      raise Exception("TermiTable: mismatch of number of columns!")
    for i in range(self.num_cols):
      self.col_info[i]['align_head'] = alignments[i]

  def set_resizable(self, colnums):
    for colnum in colnums:
      if colnum < self.num_cols:
        self.col_info[colnum]['resizable'] = True
      else: raise Exception("TermiTable: column Index out of Range!")
    self.num_resizables = len(colnums)

  def get_num_rows(self): return len(self.rows)

  def add_row( self, row ):
    if len(row) != self.num_cols:
      raise Exception("TermiTable: mismatch of number of columns in added row!")
    for i in range(self.num_cols):
      if len(row[i]) > self.col_info[i]['size']:
        self.col_info[i]['size'] = len(row[i])
    self.rows.append(row)

  def append_broadcasts( self, broadcasts, colnames ):
    assert len(colnames) == self.num_cols
    for broadcast in broadcasts:
      row = broadcast.to_list( colnames )
      for i in range(self.num_cols):
        if len(row[i]) > self.col_info[i]['size']:
          self.col_info[i]['size'] = len(row[i])
      self.rows.append(row)

  def resize_columns_to_width( self, cols, width ):
    cum_col_width = 0
    for col in cols: cum_col_width += col['size']

    if width >= cum_col_width: return

    resize_rate = width/cum_col_width
    cum_col_width = 0
    for col in cols:
      col['size'] = int(col['size'] * resize_rate)
      cum_col_width += col['size']

    resize_diff = width - cum_col_width

    for i in range(resize_diff): cols[i]['size'] += 1


  def recalculate_column_sizes(self):
    """Fit table to terminal width by resizing columns appropriately"""
    num_term_cols = TermiTable.get_term_width()
    num_term_cols = num_term_cols - len(self.col_sep)*(self.num_cols-1)
    table_width = 0
    cum_size_resizables = 0

    for col in self.col_info:
      table_width += col['size']
      if col['resizable']: cum_size_resizables += col['size']

    cols_to_resize = []
    if( table_width > num_term_cols ):
      min_table_size = table_width - cum_size_resizables
      if min_table_size > num_term_cols:
        resize_to = num_term_cols
        for col in self.col_info:
          if col['resizable']: col['size'] = 0
          else:                cols_to_resize.append(col)
      else:
        resize_to = num_term_cols - min_table_size
        for col in self.col_info:
          if col['resizable']: cols_to_resize.append(col)
      self.resize_columns_to_width( cols_to_resize, resize_to )

  def get_alignment( self, row, col ):
    if row == 0 and 'align_head' in self.col_info[col]:
      return self.col_info[col]['align_head']
    elif row != 0 and 'align' in self.col_info[col]:
      return self.col_info[col]['align']
    else: return ""

  def print_table(self):
    for j in range(len(self.rows)):
      row = self.rows[j]
      for i in range(self.num_cols):
        if len(row[i]) > self.col_info[i]['size']:
          col = row[i][0:self.col_info[i]['size']]
        else: col = row[i]
        alignment = self.get_alignment( j, i )
        fstring = "%" + alignment + str(self.col_info[i]['size']) + "s"
        if i < self.num_cols-1: fstring += self.col_sep
        print( fstring % col, end = "" )
      print("")

  @staticmethod
  def get_term_width():
    num_term_rows, num_term_cols = os.popen('stty size', 'r').read().split()
    return int(num_term_cols)

def mergedir(src, dst):
  for item in os.listdir(src):
    s = os.path.join(src, item)
    d = os.path.join(dst, item)
    if os.path.isdir(s):
      os.makedirs(d, exist_ok=True)
      mergedir(s, d)
    else:
      shutil.copy2(s, d)

class Connector():
  def __init__( self, address="www.google.com", port=80 ):
    self.address = "www.google.com"
    self.port = 80
    self.wait_for_conn_msg = "%s - waiting for connection."
    self.connect_to_internet_cmd = "ow"

  def set_address( self, address ): self.address = address
  def set_port( self, port ): self.port = port
  def set_wait_for_connection_msg( self, msg ): self.wait_for_conn_msg = msg
  def set_connect_to_internet_cmd( self, cmd ):
    self.connect_to_internet_cmd = cmd

  def is_connected( self, timeout=2 ):
    try:
      # see if we can resolve the host name
      # tells us if there is a DNS listening
      host = socket.gethostbyname( self.address )
      # connect to the host -- tells us if the host is actually reachable
      s = socket.create_connection( (host, self.port), timeout )
      return True
    except:
      pass
    return False

  def wait_until_connected( self, showtime=True, sleeptime=10, timeout=0 ):
    self.wait_starttime = datetime.datetime.now()
    while not self.is_connected():
      time.sleep( sleeptime )
      self.showtime( showtime )
      if self.test_timeout( timeout ):
        return False
    if showtime: print()
    return True

  def test_timeout( self, timeout ):
    if timeout > 0:
      delta = datetime.datetime.now() - self.wait_starttime
      if delta.total_seconds() > timeout: return True

  def showtime( self, showtime=True ):
    if showtime:
      delta = datetime.datetime.now() - self.wait_starttime
      print( self.wait_for_conn_msg % str(delta).split(".")[0], end="\r" )
  
  def keep_internet_connection_alive( self, sleeptime=10, timeout=20 ):
    while True:
      if not wait_until_connected( True, sleeptime, timeout ):
        self.connect_to_internet()
      while self.is_connected(): sleep( sleeptime )

  def connect_to_internet( self ):
    subprocess.call( self.connect_to_internet_cmd, shell=True )

class Log():
  log = None
  def __init__(self):
    self.verbosity = 2
    self.logfile = "mdl.log"

  def logme(self, text, verbosity=2, seperator=" ", end="\n"):
    if verbosity >= self.verbosity:
      with open( self.logfile, 'a' ) as log:
        stamp = datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")
        log.write( stamp + seperator + text + end )

  @staticmethod
  def get_log():
    if( not Log.log ): Log.log = Log()
    return Log.log

def logme(text, verbosity=2, seperator=" ", end="\n"):
  Log.get_log().logme(text, verbosity, seperator)
